/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('daily_jobswork', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    jobasign_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    contractor_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    crew_count: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    not_worked: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'daily_jobswork'
  });
};
