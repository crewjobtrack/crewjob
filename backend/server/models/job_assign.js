/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('job_assign', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER(11),
            allowNull: null
        },
        job_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        date_alloted: {
            type: DataTypes.DATE,
            allowNull: false
        },
        date_completed: {
            type: DataTypes.DATE,
            allowNull: true
        },
        status: {
            type: DataTypes.INTEGER(2),
            allowNull: false
        },
        comments: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'job_assign'
    });
};