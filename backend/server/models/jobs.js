/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('jobs', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        number: {
            type: DataTypes.FLOAT,
            allowNull: false,
            defaultValue: '1000',
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        location: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        latitude: {
            type: "DOUBLE",
            allowNull: true
        },
        longitude: {
            type: "DOUBLE",
            allowNull: true
        },
        type: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        start_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        end_date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        notes: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        status: {
            type: DataTypes.INTEGER(3),
            allowNull: false
        },
        bid_amount: {
            type: "DOUBLE",
            allowNull: true
        },
        user_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        city: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        state: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        zip: {
            type: DataTypes.STRING(30),
            allowNull: true
        },
        office_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'jobs'
    });
};