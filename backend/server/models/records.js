/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('records', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    job_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    not_worked: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'records'
  });
};
