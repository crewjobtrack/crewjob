import db from '../config/db'
import env from '../config/env';
import async from 'async';
import _ from 'underscore';
import hat from 'hat';
import nodemailer from 'nodemailer';
import message from '../function/messages';
import moment from 'moment';
const crypto = require('crypto');
const date = moment().format("MM/DD/YY");

module.exports = {
    /**
     * encrypt string
     * 
     **/

    encrypt: (text) => {
        const cipher = crypto.createCipher(env.CRYPTO_ALGO, env.CRYPTO_SECRET);
        let crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    },

    /**
     * encrypt string
     */

    decrypt: (text) => {
        const decipher = crypto.createDecipher(env.CRYPTO_ALGO, env.CRYPTO_SECRET);
        let dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    },
    // upper body of mail
    mailupperbody: '<table border="0" cellpadding="0" cellspacing="0" id="templateColumns" style="margin:0 auto; font-family: sans-serif;max-width: 714px;width: 100%;"><tr><td align="center" valign="top" width="50%" class="templateColumnContainer"><table border="0" cellpadding="10" cellspacing="0" width="100%" style="background-color:#fff; border:1px solid #000"><tr><td style="padding: 35px;"><table style="width:100%; float:left; width: 100%;"><tr><td style="padding: 0px 0px 0px 0px;"><p style="margin: 5px 0px;font-size: 14px;"><b>Greeting !!!</b></p><p style="margin: 0px 0px;font-size: 14px;">Wellcome to <a href="http://104.131.20.101/frontend/#/" style="text-decoration: none">CREWJOB.com</a></p><p style="margin: 15px 0px;font-size: 14px;">Wising you a great future and long relationship with <a href="http://104.131.20.101/frontend/#/" style="text-decoration: none">CREWJOB.COM</a> Ltd  Team.</p>',

    // lower body of mail
    maillowerbody: ' <p style="margin: 5px 0px;font-size: 14px;"><b>Please visit us.... <a href="http://104.131.20.101/frontend/#/" style="text-decoration: none">CREWJOB.com</a></b></p><p style="margin: 30px 0px 0;font-size: 14px;"><b>Yours,</b></p><p style="margin: 5px 0px;font-size: 14px;"><a href="http://104.131.20.101/frontend/#/" style="text-decoration: none">CREWJOB.com</a></p></td></tr></table></tr></td></table></td></tr></table>',


    /**
     * mail function
     */

    // every mail send by using this function
    mail: (mailDetail, cb) => {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 25,
            auth: {
                user: env.MAIL_ID,
                pass: env.MAIL_PASS
            }
        });
        let mailOptions = {
            from: mailDetail.from,
            to: mailDetail.to,
            subject: mailDetail.subject,
            text: mailDetail.text,
            html: mailDetail.html
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                var msg = {
                    status: false
                }
                return cb(msg);
            } else {

                var msg = {
                    status: true
                }
                return cb(msg);

            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    },
    mailUser: (email, name, pass, cb) => {
        console.log('in mailUser' + email);
        const uBody = module.exports.mailupperbody;
        const lBody = module.exports.maillowerbody;
        const link = '<p style="margin: 15px 0px;font-size: 14px;"><b>Please Login by Following Link</b></p><p style="margin: 5px 0px;font-size:14px"><b>YourUsername=<a href="" style="text-decoration:none">' + email + '</a></b></p><p style="margin: 5px 0px;font-size: 14px;"><b>Your Password=' + pass + '</b></p>';
        const mailDetail = {
            name: name,
            from: env.MAIL_ID,
            to: email,
            subject: 'Member added notification',
            text: 'Construction work tracker',
            html: uBody + link + lBody
        }
        module.exports.mail(mailDetail, (response) => {
            if (response.status == false) {
                var msg = {
                    status: false
                }
                return cb(msg);
            } else {
                var msg = {
                    status: true
                }
                return cb(msg);
            }
        });
    },
    forgetPassword: (token, email, cb) => {
        console.log('in mailUser' + email);
        const uBody = module.exports.mailupperbody;
        const lBody = module.exports.maillowerbody;
        const link = '<p style="margin: 15px 0px;font-size: 14px;"><b>Please reset your password by Following Link</b></p><a href="http://104.131.20.101/frontend/#/resetpassword?token=' + token + '" style="text-decoration: none">http://104.131.20.101/frontend/#/resetpassword?token=' + token + '</a></b></p><p style="margin: 30px 0px 0;font-size: 14px;"><b>Yours,</b></p><p style="margin: 5px 0px;font-size: 14px;">'
        const mailDetail = {
            from: env.MAIL_ID,
            to: email,
            subject: 'Forgot Password',
            text: 'Construction work tracker',
            html: uBody + link + lBody
        }
        module.exports.mail(mailDetail, (response) => {
            if (response.status == false) {
                var msg = {
                    status: false
                }
                return cb(msg);
            } else {
                var msg = {
                    status: true
                }
                return cb(msg);
            }
        });
    },

    lead: (name, address, contactNo, officer_id, lat, lng, cb) => {
        console.log(name);
        const uBody = module.exports.mailupperbody;
        const lBody = module.exports.maillowerbody;
        db.users.findAll({
            where: {
                $or: [{
                        id: officer_id
                    },
                    {
                        type: 1
                    }
                ]
            },
            raw: true,
            attributes: ['email']
        }).then(email => {
            const emails = _.pluck(email, 'email');
            console.log(emails);
            const html = '<p> New lead assign to you.<br><ul>            <li><p>Name: ' + name + '</p></li>            <li><p>Address: ' + address + '</P></li>            <li><p>Contact Number: ' + contactNo + '</p></li>          </ul>  Review full detail at your account</p><p>Please <a href="https://www.google.com/maps/?q=' + lat + ',' + lng + '" style="text-decoration: none"><u>Click Here</u></a> for view job location</p>';
            const mailDetail = {
                from: env.MAIL_ID,
                to: emails,
                subject: 'New Lead',
                text: 'Construction work tracker',
                html: uBody + html + lBody
            }
            module.exports.mail(mailDetail, (response) => {
                if (response.status == false) {
                    var msg = {
                        status: false
                    }
                    return cb(msg);
                } else {
                    var msg = {
                        status: true
                    }
                    return cb(msg);
                }
            });
        }).catch(e => cb(e));
    },

    workMail: (email, name, datetime, latlong, conname, jobtitle, jobnum, crew, cb) => {
        console.log('in workMail' + email + name + datetime + latlong + conname + jobtitle + jobnum);
        const uBody = '<table border="0" cellpadding="0" cellspacing="0" id="templateColumns" style="margin:0 auto; font-family: sans-serif;max-width: 767px;width: 100%;"><tr><td align="center" valign="top" width="50%" class="templateColumnContainer"><table border="0" cellpadding="10" cellspacing="0" width="100%" style="background-color:#fff; border:1px solid #000"><tr><td style="padding: 15px 30px;"><table style="width:100%; float:left; width: 100%;"><tr><td style="padding: 0px 0px 15px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/jobtrackremail.png" style="vertical-align: middle; display: inline-block;width: 25%; margin-right: 10px"><h3 style="margin: 0px 0px 0px; color: #3b5998; vertical-align: middle; display: inline-block;"><b>Job Alert</b></h3></td></tr><tr><td style="padding: 20px 0px 15px 0px;border-bottom:1px solid #ccc"><p style="margin: 0px 0px 15px;font-size: 16px;"><b>Hi!  ' + name + '</b></p><p style="margin: 0px 0px 15px;font-size: 16px;">A Job has been started  with the following details </p></td></tr>';
        const lBody = ' <tr><td class="leftColumnContent" style=";"><button style="color: #fff;border: 2px solid #3b5998;background: #3b5998;font-size: 20px;padding: 10px 20px;margin:10px 0;border-radius: 3px !important;">See Jobs</button></td></tr></table></tr></td></table></td></tr></table>';
        const time = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/clock-time-2.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b>' + datetime + '</b></p></td></tr>';
        const location = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/loc.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b> ' + latlong + '</b></p></td></tr>';
        const title = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/loc.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b>Job: ' + jobtitle + '</b></p></td></tr>';
        const number = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/loc.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b>Job Code: ' + jobnum + '</b></p></td></tr>';
        const contractor = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/loc.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b>Contractor name: ' + conname + '</b></p></td></tr>';
        const crewno = '<tr><td style="padding: 20px 0px;border-bottom:1px solid #ccc"><img src="http://104.131.20.101/frontend/images/loc.png" style="vertical-align: middle; display: inline-block;width: 3%; margin-right: 10px"><p style="margin: 0px;font-size: 16px;vertical-align: middle; display: inline-block"><b>Workers today: ' + crew + '</b></p></td></tr>';
        const mailDetail = {
            name: name,
            from: env.MAIL_ID,
            to: email,
            subject: 'Job notification',
            text: 'Construction work tracker',
            html: uBody + time + location + title + number + contractor + crewno + lBody
        }
        module.exports.mail(mailDetail, (response) => {
            if (response.status == false) {
                var msg = {
                    status: false
                }
                return cb(msg);
            } else {
                var msg = {
                    status: true
                }
                return cb(msg);
            }
        });
    },
    /**
     * UPDATE META
     */
    updateMeta: (data, id) => {
        let check = [];
        for (var key in data) {
            let metakey = key;
            let metavalue = data[key];
            console.log('Meta key here');
            console.log(metakey);
            console.log('Meta value here');
            console.log(metavalue);
            db.user_meta.update({
                meta_value: metavalue
            }, {
                where: {
                    meta_key: metakey,
                    user_id: id
                }
            }).then((success) => {
                check.push(success);
            }).catch(e => next(e));
        }
        if (check === null) {
            return false;
        } else {
            return true;
        }
    }

}