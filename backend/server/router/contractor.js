import express from 'express';
import Contracter from '../controller/contractor';
import {
    loginRequired
} from '../controller/admin';

const router = express.Router();


router.route('/signin')
    /* POST /v1.0/contractor/signin - Contracter Login */
    .post(Contracter.signinContracter);
router.route('/changepassword')
    /* POST /v1.0/contractor/signin - Contracter change password */
    .post(loginRequired, Contracter.changePassword);
router.route('/myjobs/:id')
    /* GET /v1.0/contractor/myjobs/:id - Jobs under Contracter PENDING */
    .get(loginRequired, Contracter.mypenJobs)
    /* POST /v1.0/contractor/myjobs/:id - Jobs under Contracter COMPLETED */
    .post(loginRequired, Contracter.mycomJobs);

export default router;