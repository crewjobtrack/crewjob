import express from 'express';
import Officer from '../controller/officer';
import {
    loginRequired
} from '../controller/admin';

const router = express.Router();


router.route('/signin')
    /* POST /v1.0/officer/signin - Officer Login */
    .post(Officer.signinOfficer);
router.route('/viewofficer/:id')
    /* GET /v1.0/officer/viewofficer - GET A OFFICER */
    .get(loginRequired, Officer.getOfficer)
    /* POST /v1.0/officer/viewofficer - UPDATE OFFICER */
    .post(loginRequired, Officer.updateOfficer);

router.route('/forgetPassword')
    /* POST /v1.0/officer/forgetpassword - Forget password */
    .post(loginRequired, Officer.forgetPass)
    /* PUT /v1.0/officer/forgetpassword - Reset password */
    .put(loginRequired, Officer.resetPass);

router.route('/verifyToken')
    /* POST /v1.0/officer/verifyToken - Verify token */
    .post(loginRequired, Officer.verifyToken);

router.route('/credentials')
    /* POST /v1.0/officer/credentials - Change password*/
    .post(loginRequired, Officer.checkPassword)
    .put(loginRequired, Officer.changePassword);
router.route('/manageofficer/:id')
    /* GET /v1.0/officer/manageofficer/:id- GET  OFFICER OF OFFICES */
    .get(loginRequired, Officer.officeOfficer);
router.route('/activejobs/:id')
    /* GET /v1.0/officer/activejobs/:id- GET  ACTIVE JOBS OF  A OFFICES */
    .get(loginRequired, Officer.activeJobs);


export default router;