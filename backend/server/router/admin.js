import express from 'express';
import Admin from '../controller/admin';
import {
    loginRequired
} from '../controller/admin';

const router = express.Router();
router.route('/webmaster')
    /* POST /v1.0/admin/webmaster - Webmaster Login */
    .post(Admin.signinMaster);
router.route('/office')
    /* GET /v1.0/admin/office - GET ALL OFFICES */
    .get(loginRequired, Admin.getallOffice)
    /* POST /v1.0/admin/office - ADD NEW OFFICE */
    .post(loginRequired, Admin.addOffice);

router.route('/archiveJob/:id')
    /* PUT /v1.0/admin/archiveJob/:id - Add job in archive */
    .put(loginRequired, Admin.archiveJob);

router.route('/assignedContractor/:jobId')
    /* Get /v1.0/admin/assignedContractor/:jobId - Get assigned contractor */
    .get(loginRequired, Admin.assignedContractor);

router.route('/jobType')
    /* GET /v1.0/admin/jobType - Get all jobType */
    .get(loginRequired, Admin.getJobTypes)

    /* POST /v1.0/Admin/jobType - Create jobType */
    .post(loginRequired, Admin.addJobType);


router.route('/lead')
    /* POST /v1.0/admin/lead - Add lead */
    .post(loginRequired, Admin.addLead)

    /* PUT /v1.0/admin/lead - Edit lead */
    .put(loginRequired, Admin.editLead)

    /* GET /v1.0/admin/lead - Get all lead */
    .get(loginRequired, Admin.getLeads);


router.route('/note')
    /* POST /v1.0/admin/addNote - Add note */
    .post(loginRequired, Admin.addNote)

    /* PUT /v1.0/admin/addNote - Edit note */
    .put(loginRequired, Admin.editNote);

router.route('/leadStatus/:id')
    /* PUT /v1.0/admin/leadStatus - Edit status */
    .put(loginRequired, Admin.leadStatus);

router.route('/lead/:id')
    /* GET /v1.0/admin/lead/:id - Get lead */
    .get(loginRequired, Admin.getLead);

router.route('/jobType/:id')
    /* PUT /v1.0/Admin/jobType - Edit jobType by jobId */
    .put(loginRequired, Admin.editJobType)

    /* DELETE /v1.0/Admin/jobType - Delete jobType by jobId */
    .delete(Admin.deleteJobType);

router.route('/viewoffice/:id')
    /* GET /v1.0/admin/viewoffice - GET OFFICE DETAILS*/
    .get(loginRequired, Admin.getOffice);
router.route('/editoffice/:id')
    /* GET /v1.0/admin/editoffice/:id - DELETE OFFICE */
    .get(loginRequired, Admin.deleteOffice)
    /* POST /v1.0/admin/editoffice/:id - UPDATE OFFICE */
    .post(loginRequired, Admin.updateOffice);

router.route('/user')
    /* GET /v1.0/admin/user - GET ALL USER */
    .get(loginRequired, Admin.getallUser)
    /* POST /v1.0/admin/user- ADD NEW USER */
    .post(loginRequired, Admin.createUser);

router.route('/user/adddetail')
    /* POST /v1.0/admin/user- ADD MORE USER DETAILS */
    .post(loginRequired, Admin.moreUserDetail);
router.route('/viewuser/:id')
    /* GET /v1.0/admin/user - GET A USER */
    .get(loginRequired, Admin.getUser)
    /* POST /v1.0/admin/user- UPDATE A USER */
    .post(loginRequired, Admin.updateUser)
    /* PUT /v1.0/admin/user - Update user profileImage */
    .put(loginRequired, Admin.updateUserProfileImg);

router.route('/officer')
    /* GET /v1.0/admin/officer - GET ALL OFFICER */
    .get(loginRequired, Admin.getallOfficer);
router.route('/viewofficer/:id')
    /* GET /v1.0/admin/officer - GET ALL OFFICER */
    .get(loginRequired, Admin.getOfficer);
router.route('/editofficer/:id')
    /* GET /v1.0/admin/editofficer - DELETE OFFICER */
    .get(loginRequired, Admin.deleteOfficer);
router.route('/contractor')
    /* GET /v1.0/admin/contractor - GET ALL CONTRACTOR */
    .get(loginRequired, Admin.getallContractor)
    /* POST /v1.0/admin/contractor-GET A CONTRACTOR DETAILS */
    .post(loginRequired, Admin.getContractor);
router.route('/editcontractor/:id')
    /* GET /v1.0/admin/editcontractor - DELETE CONTRACTOR */
    .get(loginRequired, Admin.deleteContractor)
    /* POST /v1.0/admin/editcontractor- UPDATE CONTRACTOR DETAILS */
    .post(loginRequired, Admin.updateContractor);
router.route('/payment')
    /* GET /v1.0/admin/payment - GET ALL PAYMENTS */
    .get(loginRequired, Admin.getPayments)
    /* POST /v1.0/admin/payment- ADD NEW PAYMENT */
    .post(loginRequired, Admin.addPayment);
router.route('/viewpayment/:id')
    /* GET /v1.0/admin/viewpayment - GET A PAYMENT */
    .get(loginRequired, Admin.getPayment);
router.route('/editpayment/:id')
    /* GET /v1.0/admin/editpayment - DELETE PAYMENT */
    .get(loginRequired, Admin.deletePayment)
    /* POST /v1.0/admin/editpayment- UPDATE PAYMENT */
    .post(loginRequired, Admin.updatePayment);

router.route('/changePassword/:id')
    /* POST /v1.0/admin/changePassword/:id - Confirm current password */
    .post(loginRequired, Admin.confirmPassword)

    /* PUT /v1.0/admin/changePassword/:id - Reset password of  user */
    .put(loginRequired, Admin.changePassword);

router.route('/exists')
    /* POST /v1.0/admin/exits- TO CHECK IF EMAIL EXISTS */
    .post(loginRequired, Admin.ifExists);
router.route('/getadmin/:id')
    /* GET /v1.0/admin/getadmin - GET ALL ADMINS */
    .get(loginRequired, Admin.getallAdmins);
router.route('/admin/:id')
    /* GET /v1.0/admin/admin - GET A ADMINS */
    .get(loginRequired, Admin.getAdmin);
router.route('/editadmin/:id')
    /* GET /v1.0/admin/editadmin -  DELETE ADMIN */
    .get(loginRequired, Admin.delAdmin)
    /* POST /v1.0/admin/editadmin -  UPDATE ADMIN */
    .post(loginRequired, Admin.updateAdmin);


router.route('/test')
    .post(loginRequired, Admin.test);

export default router;