import express from 'express';
import Jobs from '../controller/jobs';
import {
    loginRequired
} from '../controller/admin';

const router = express.Router();

router.route('/jobs')
    /* GET /v1.0/jobs/jobs - GET ALL JOBS */
    .get(loginRequired, Jobs.getallJobs)
    /* POST /v1.0/jobs/jobs- ADD JOB */
    .post(loginRequired,Jobs.addJob);
router.route('/viewjob/:id')
    /* GET /v1.0/jobs/jobs - GET A JOB DETAILS */
    .get(loginRequired, Jobs.getJob);


router.route('/contractorJobs/:id')
    /* GET /v1.0/jobs/contractorJobs/:id - Get contractor job history */
    .get(loginRequired, Jobs.contractorJobs);

router.route('/assignedJobTypes/:id')
    /* GET /v1.0/jobs/jobs - Get all job types of job */
    .get(loginRequired, Jobs.assignedJobTypes);

router.route('/editjobs/:id')
    /* POST /v1.0/jobs/editjobs- UPDATE JOB DETAILS */
    .post(loginRequired,Jobs.updateJob);

router.route('/deleteJob/:id')
    /* GET /v1.0/jobs/deleteJob/:id - DELETE JOB */
    .delete(loginRequired, Jobs.deleteJob);

router.route('/assign')
    /* POST /v1.0/jobs/editjobs- ASSIGN JOB TO CONTRACTER*/
    .post(loginRequired,Jobs.assignJob);
router.route('/clockin')
    /* POST /v1.0/jobs/clockin- JOB STARTED */
    .post(loginRequired,Jobs.clockIn);

router.route('/jobstype/:type')
    /* GET /v1.0/jobs/jostype?;type - GET ALL JOBS BY TYPE */
    .get(loginRequired, Jobs.jobsType);

router.route('/timeJob/:id')
    /* POST /v1.0/jobs/editTimeJob - Add time job */
    .post(loginRequired,Jobs.addTimeJob)
    /* PUT /v1.0/jobs/editTimeJob - Edit time job */
    .put(loginRequired, Jobs.editTimeJob);

router.route('/clockout')
    /* POST /v1.0/jobs/clockout - JOB ENDED */
    .post(loginRequired,Jobs.clockOut);
router.route('/notworked')
    /* POST /v1.0/jobs/notworked- NOT WORKING TODAY */
    .post(loginRequired,Jobs.notWork);
router.route('/viewrecords')
    /* POST /v1.0/jobs/viewrecords- VIEWING RECORDS */
    .post(loginRequired,Jobs.viewRecords);
router.route('/status/:id')
    /* POST /v1.0/jobs/status- VIEWING STATUS */
    .get(loginRequired, Jobs.status);
router.route('/activejobs')
    .get(loginRequired, Jobs.activeJobs);

router.route('/notes')
    /* POST /v1.0/jobs/notes- Add job Notes */
    .post(loginRequired,Jobs.addJobNotes)

    /* PUT /v1.0/jobs/notes - Edit job notes */
    .put(loginRequired, Jobs.editJobNotes);

router.route('/notes/:jobId')
    /* GET /v1.0/jobs/:jobId - Get all notes by jobId */
    .get(loginRequired, Jobs.getAllJobNotes)

    /* DELTE /v1.0/jobs/:jobId -Delete note by id */
    .delete(loginRequired, Jobs.deleteJobNote);

router.route('/lastnumber')
    /* GET /v1.0/jobs/lastnumber- VIEWING LAST JOB NUMBER */
    .get(loginRequired, Jobs.lastNum);
router.route('/filter')
    /* POST /v1.0/jobs/filter- VIEWING JOBS BY TYPE OR CREW FILTER */
    .post(loginRequired,Jobs.filterJob);
router.route('/currentjobs')
    /* GET /v1.0/jobs/currentjobs- VIEWING JOBS BY WHETHER ACTIVE OR NOT */
    .get(loginRequired, Jobs.currentJobs);
// git status && git commit -am "add job" && git push origin server && git checkout master && git pull origin master && git pull origin server && git push origin master && git checkout server && git pull origin master 

export default router;