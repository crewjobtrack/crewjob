import hat from 'hat';
import db from '../config/db';
import env from '../config/env';
import async from 'async';
import moment from 'moment';
import message from '../function/messages';
import helpme from '../function/helpme';
import _ from 'underscore';
const crypto = require('crypto');

const date = new Date();

/**
 * ASSIGNING JOBS
 * 
 */
function assignJob(req, res, next) {
    const assign = {
        user_id: req.body.user_id,
        job_id: req.body.job_id,
        date_alloted: req.body.date_alloted,
        date_completed: req.body.date_completed || null,
        status: 1,
        comments: req.body.comments || null
    }
    db.job_assign.find({
        where: {
            job_id: req.body.job_id,
            date_completed: null,
            status: {
                $not: 1
            }
        }
    }).then((penjob) => {
        if (penjob === null) {
            db.job_assign.create(assign).then((assign) => {
                if (assign == null) {
                    const msg = {
                        status: false,
                        msg: message.notCreate
                    };
                    res.json(msg);
                } else {
                    const msg = {
                        status: true,
                        msg: message.create
                    };
                    res.json(msg);
                }
            }).catch(e => next(e));
        } else {
            db.job_assign.update({
                date_completed: req.body.date_alloted,
                status: 0
            }, {
                where: {
                    id: penjob.id
                }
            }).then((update) => {
                db.job_assign.create(assign).then((assign) => {
                    if (assign == null) {
                        const msg = {
                            status: false,
                            msg: message.notCreate
                        };
                        res.json(msg);
                    } else {
                        const msg = {
                            status: true,
                            msg: message.create
                        };
                        res.json(msg);
                    }
                }).catch(e => next(e));
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}

/**
 * ADDING NEW JOB
 * 
 */
function addJob(req, res, next) {
    const job = {
        title: req.body.title,
        number: req.body.number,
        location: req.body.location,
        address: req.body.address,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        type: req.body.type,
        start_date: req.body.start_date,
        end_date: req.body.end_date || null,
        notes: req.body.notes,
        status: req.body.status,
        bid_amount: req.body.bid_amount,
        user_id: req.body.user_id,
        office_id: req.body.office_id,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip
    }
    db.jobs.create(job).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notCreate
            };
            res.json(msg);
        } else {
            if (req.body.contractor_id === null) {
                const msg = {
                    status: true,
                    msg: message.create
                };
                res.json(msg);
            } else {
                async.each(req.body.contractors, (contractor, callback) => {
                    db.job_assign.create({
                        user_id: contractor.contractor_id,
                        job_id: success.id,
                        date_alloted: success.created_at,
                        date_completed: req.body.date_completed || null,
                        status: 0,
                        comments: req.body.comments || null
                    }).then((assign) => {
                        callback();
                    }).catch(e => next(e));
                }, (err) => {
                    if (err) {
                        const msg = {
                            status: true,
                            msg: message.notCreate
                        };
                    } else {
                        const msg = {
                            status: true,
                            msg: message.create
                        };
                        res.json(msg);
                    }
                });

            }
        }
    }).catch(e => next(e));
}
/**
 * VIEWING ALL JOBS
 * 
 */
function getallJobs(req, res, next) {

    db.jobs.findAll({

        raw: true
    }).then((jobs) => {
        if (jobs === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(jobs, (job, callback) => {
                let row = {};
                row = job;
                db.office.find({
                    where: {
                        id: job.office_id
                    },
                    attributes: ['name'],
                    raw: true
                }).then((office) => {
                    if (office === null) {
                        result.push(row);
                        callback();
                    } else {
                        row.office_name = office.name;
                        db.job_assign.findAll({
                            where: {
                                job_id: job.id,
                                status: 0
                            },

                            raw: true
                        }).then((job_assign) => {
                            if (job_assign === null) {
                                result.push(row);
                                callback();
                            } else {
                                getContractor(job_assign, (name) => {
                                    row.contractor_name = name;
                                    result.push(row);
                                    callback();
                                });
                            }
                        }).catch(e => next(e));
                    }
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));

}

/**
 * VIEWING ALL JOBS BY TYPE
 * 
 */
function jobsType(req, res, next) {
    let type = req.params.type;
    type = (type == 0) ? [2] : [0, 1];
    db.jobs.findAll({
        where: {
            status: {
                $in: type
            }
        },
        raw: true
    }).then((jobs) => {
        if (jobs === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(jobs, (job, callback) => {
                let row = {};
                row = job;
                db.office.find({
                    where: {
                        id: job.office_id
                    },
                    attributes: ['name'],
                    raw: true
                }).then((office) => {
                    if (office === null) {
                        result.push(row);
                        callback();
                    } else {
                        row.office_name = office.name;
                        db.job_assign.findAll({
                            where: {
                                job_id: job.id,
                                status: 0
                            },

                            raw: true
                        }).then((job_assign) => {
                            if (job_assign === null) {
                                result.push(row);
                                callback();
                            } else {
                                getContractor(job_assign, (name) => {
                                    row.contractor_name = name;
                                    result.push(row);
                                    callback();
                                });
                            }
                        }).catch(e => next(e));
                    }
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));

}

/**
 *assigned job types
 * 
 */
function assignedJobTypes(req, res, next) {
    db.jobs.find({
        where: {
            id: req.params.id
        },
        attributes: ['type'],
        raw: true
    }).then((jobTypes) => {
        let array;
        array = JSON.parse(jobTypes.type);
        let result = [];
        async.each(array, (type, callback) => {
            let row = {};
            row = (_.object(['title'], [type]));
            result.push(_.extend(row, {
                status: 1
            }));
            callback();
        }, (err) => {
            if (err) {
                console.log('Error grabbing data');
            } else {
                //res.json(result)
                db.job_type.findAll({
                    attributes: ['title'],
                    raw: true
                }).then((success) => {
                    let resultArray = _.union(result, success)
                    let uniqueLists = _.uniq(resultArray, function (item, key, title) {
                        return item.title
                    });
                    res.json(uniqueLists);
                    console.log('Finished processing all data');
                }).catch(e => next(e));
            }
        });
    }).catch(e => next(e));
}


function getContractor(job_assign, next) {
    let result = [];
    async.each(job_assign, (jobAsign, callback) => {
        db.users.find({
            where: {
                id: jobAsign.user_id
            },
            attributes: ['name', 'last_name'],
            raw: true
        }).then((user) => {
            console.log(user);
            if (user === null) {
                callback();
            } else {
                let row = {};
                console.log('33');
                console.log(user);
                row.contractor_name = user.name + ' ' + user.last_name;
                result.push(row);
                callback();
            }
        }).catch(e => next(e));
    }, (err) => {
        if (err) {
            console.log('Error grabbing data');
        } else {
            return next(result);
            console.log('Finished processing all data');
        }
    });

}


function contractorJobs(req, res, next) {
    let resultArray = [];
    db.job_assign.findAll({
        where: {
            user_id: req.params.id
        },
        raw: true
    }).then(jobs => {
        let row = {};
        async.each(jobs, function (job, callback) {
            jobDetail(job.job_id, (details) => {
                row = details;
                resultArray.push(row);
                callback();
            });
        }, function (err) {
            if (err) {
                console.log("Error grabbing data");
            } else {
                res.json(resultArray);
                console.log("Finished processing all data");
            }
        });
    }).catch(e => next(e));
}


function getJob(req, res, next) {
    jobDetail(req.params.id, (details) => {
        res.json(details);
    });
}
/**
 * VIEWING JOB DETAILS
 * 
 */
function jobDetail(id, next) {
    db.jobs.find({
        where: {
            id: id
        },
        raw: true
    }).then((job) => {
        if (job === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            return next(msg);
        } else {
            let row = {};
            row = job;
            db.job_assign.findAll({
                where: {
                    job_id: id
                },
                raw: true
            }).then((assign) => {
                if (assign === null) {
                    return next(row);
                } else {
                    row.assign = assign;
                    db.daily_jobswork.findAll({
                        where: {

                            jobasign_id: _.pluck(assign, 'id')
                        },
                        order: [
                            ['created_at', 'DESC']
                        ],
                        raw: true
                    }).then((dailyrecords) => {
                        let records = [];
                        row.records = [];
                        async.each(dailyrecords, (dailyrecord, callback) => {

                            db.users.find({
                                where: {
                                    id: dailyrecord.contractor_id
                                },
                                attributes: ['name', 'last_name'],
                                raw: true
                            }).then((record) => {
                                let startT = moment(dailyrecord.created_at);
                                let endT = moment(dailyrecord.updated_at);
                                let hours = endT.diff(startT, 'hours') + 'Hours' + endT.diff(startT, 'minutes') + 'Minutes';
                                records.push(_.extend(dailyrecord, {
                                    hours_worked: hours
                                }, record));
                                row.records = records;
                                callback();
                            }).catch(e => next(e));
                        }, (err) => {
                            if (err) {
                                console.log('Error grabbing data');
                            } else {
                                return next(row);
                                console.log('Finished processing all data');
                            }
                        });
                    }).catch(e => next(e));
                }
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}
/**
 * UPDATE JOB
 * 
 */
function updateJob(req, res, next) {
    db.jobs.update(req.body, {
        where: {
            id: req.params.id
        },
        raw: true
    }).then((job) => {
        if (job === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            db.job_assign.update({
                status: 1
            }, {
                where: {
                    job_id: req.params.id
                }
            }).then((job_assign) => {
                let ids = _.pluck(req.body.contractors, 'contractor_id');
                async.each(ids, (id, callback) => {
                    db.job_assign.findOrCreate({
                        where: {
                            job_id: req.params.id,
                            user_id: id,
                        },
                        defaults: {
                            status: 0,
                            date_alloted: new Date()
                        }
                    }).then((user) => {
                        if (user !== null) {
                            db.job_assign.update({
                                status: 0
                            }, {
                                where: {
                                    user_id: id
                                }
                            }).then((users) => {
                                const msg = {
                                    status: true,
                                    msg: message.update
                                }
                                console.log(msg);
                            }).catch(e => next(e));
                        }
                        callback();
                    }).catch(e => next(e));
                }, (err) => {
                    if (err) {
                        const msg = {
                            status: true,
                            msg: message.notCreate
                        };
                    } else {
                        const msg = {
                            status: true,
                            msg: message.update
                        }
                        res.json(msg);
                    }
                });
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}
/**
 * DELETE JOB
 * 
 */
function deleteJob(req, res, next) {
    db.job_assign.findAll({
        where: {
            job_id: req.params.id
        },
        raw: true,
        attributes: ['id']
    }).then((found) => {
        db.jobs.destroy({
            where: {
                id: req.params.id
            },
            raw: true
        }).then((job) => {
            if (found.length === 0) {
                const msg = {
                    status: true,
                    msg: message.delete
                };
                res.json(msg);
            } else {
                db.job_assign.destroy({
                    where: {
                        job_id: req.params.id
                    },
                    raw: true
                }).then(delJobAssign => {
                    const ids = _.pluck(found, 'id');
                    db.daily_jobswork.destroy({
                        where: {
                            jobasign_id: {
                                $in: ids
                            }
                        },
                        raw: true
                    }).then(() => {
                        const msg = {
                            status: true,
                            msg: message.delete
                        };
                        res.json(msg);
                    }).catch(e => next(e));
                }).catch(e => next(e));
            }
        }).catch(e => next(e));
    }).catch(e => next(e));
}
/**
 * JOB STARTED FOR THE DAY
 * 
 */
function clockIn(req, res, next) {
    const officeid = req.body.officeid;
    db.daily_jobswork.create({
        jobasign_id: req.body.jobasign_id,
        contractor_id: req.body.contractor_id,
        crew_count: req.body.crew_count,
        active: 1,
        updated_at: null
    }, {
        silent: true,
        fields: ['updated_at', 'jobasign_id', 'contractor_id', 'crew_count', 'active']
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.tryAgain
            };
            res.json(msg);
        } else {
            var latlong;
            var conname;
            var jobtitle;
            var jobnum;
            db.job_assign.find({
                where: {
                    id: success.jobasign_id
                },
                attributes: ['job_id'],
                raw: true
            }).then((jobassign) => {
                db.jobs.find({
                    where: {
                        id: jobassign.job_id
                    },
                    raw: true
                }).then((jobs) => {
                    latlong = jobs.address;
                    jobtitle = jobs.title;
                    jobnum = jobs.number;
                    db.users.find({
                        where: {
                            id: success.contractor_id
                        },
                        attributes: ['name', 'last_name'],
                        raw: true
                    }).then((con) => {
                        conname = con.name + ' ' + con.last_name;
                    });

                    db.users.findAll({
                        where: {
                            $or: {
                                type: 1,
                                // $and: {
                                //     type: 2,
                                //     office_id: officeid
                                // }
                            }
                        },
                        raw: true
                    }).then((users) => {
                        async.each(users, (user, callback) => {
                            let time = moment.unix(success.created_at).format('dddd, MMMM Do, h:mm A');

                            helpme.workMail(user.email, user.name, time, latlong, conname, jobtitle, jobnum, req.body.crew_count, (msg) => {
                                console.log(msg);
                                callback();
                            });
                        }, (err) => {
                            if (err) {
                                console.log('Error grabbing data');
                            } else {
                                const msg = {
                                    status: true,
                                    msg: message.clockIn,
                                    id: success.id,
                                    job_id: success.id
                                };
                                res.json(msg);
                                console.log('Finished processing all data');
                            }
                        });
                    }).catch(e => next(e));
                }).catch(e => next(e));
            }).catch(e => next(e));


        }
    }).catch(e => next(e));
}
/**
 * JOB ENDED FOR THE DAY
 * 
 */
function clockOut(req, res, next) {
    db.daily_jobswork.update({
        active: 0
    }, {
        where: {
            id: req.body.id
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.tryAgain
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.clockOut
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * NOT WORKED FOR THE DAY
 * 
 */
function notWork(req, res, next) {
    db.daily_jobswork.create({
        active: 0,
        not_worked: req.body.reason,
        jobasign_id: req.body.jobasign_id,
        contractor_id: req.body.contractor_id,
        crew_count: 0
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.tryAgain
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * ACTIVE JOBS FOR THE DAY
 * 
 */
function activeJobs(req, res, next) {
    db.daily_jobswork.findAll({
        where: {
            active: 1
        },
        raw: true
    }).then((actives) => {
        if (actives === null) {
            const msg = {
                status: false,
                msg: message.tryAgain
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(actives, (active, callback) => {
                let row = {};
                row = active;
                db.users.find({
                    where: {
                        id: active.contractor_id
                    },
                    attributes: ['name', 'last_name'],
                    raw: true
                }).then((user) => {
                    row.contractor_name = user.name + ' ' + user.last_name;
                    db.job_assign.find({
                        where: {
                            id: active.jobasign_id
                        },
                        attributes: ['job_id'],
                        raw: true
                    }).then((assign) => {

                        db.jobs.find({
                            where: {
                                id: assign.job_id,
                                status: {
                                    $in: [0, 1]
                                }
                            },
                            attributes: ['id', 'number', 'title', 'address', 'latitude', 'longitude', ],
                            raw: true
                        }).then((job) => {
                            if (job == null) {
                                callback();
                            } else {
                                row.job = job;
                                result.push(row);
                                callback();
                            }
                        }).catch(e => next(e));
                    }).catch(e => next(e));
                }).catch(e => next(e));

            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });

        }
    }).catch(e => next(e));
}
/**
 * view records
 * 
 */
function viewRecords(req, res, next) {
    db.daily_jobswork.findAll({
        where: {
            jobassign_id: req.body.id
        },
        order: [
            ['created_at', 'DESC']
        ],
        raw: true
    }).then((jobs) => {
        if (jobs === null) {
            const msg = {
                status: false,
                msg: message.tryAgain
            };
            res.json(msg);
        } else {

            res.json(jobs);
        }
    }).catch(e => next(e));
}

/**
 * view status
 * 
 */
function status(req, res, next) {
    db.daily_jobswork.find({
        where: {
            jobasign_id: req.params.id,
            active: 1
        },
        attributes: ['active'],
        raw: true
    }).then((status) => {
        if (status === null) {
            const msg = {
                status: false
            };
            res.json(msg);
        } else {
            const msg = {
                status: true
            };
            res.json(msg);
        }

    });
}

function addJobNotes(req, res, next) {
    const post = req.body;
    db.job_notes.create(post)
        .then(notes => {
            let msg = {
                status: true,
                msg: message.noteAdded
            }
            res.json(msg);
        }).catch(e => next(e));

}

function getAllJobNotes(req, res, next) {
    db.job_notes.findAll({
            where: {
                job_id: req.params.jobId
            },
            order: [
                ['created_at', 'DESC']
            ],
            raw: true
        })
        .then(notes => {
            let result = [];
            async.each(notes, (note, callback) => {
                let row = {};
                db.users.find({
                    where: {
                        id: note.user_id
                    },
                    attributes: ['name', 'type', 'last_name'],
                    raw: true
                }).then((user) => {
                    console.log(user);
                    row = note;
                    row.name = user.name + ' ' + user.last_name;
                    row.type = user.type;
                    result.push(row);
                    callback();
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }).catch(e => next(e));

}

function editJobNotes(req, res, next) {
    const post = req.body;
    db.job_notes.update(post, {
            where: {
                id: req.body.id
            }
        })
        .then(priceAdded => {
            let msg = {
                status: true,
                msg: message.noteEdited
            }
            res.json(msg);
        }).catch(e => next(e));

}


function editTimeJob(req, res, next) {
    const post = req.body;
    db.daily_jobswork.update(post, {
            where: {
                id: req.params.id
            },
            silent: true,
            fields: ['updated_at', 'created_at', 'active', 'contractor_id', 'crew_count', 'not_worked']
        })
        .then(() => {
            let msg = {
                status: true,
                msg: message.editTime
            }
            res.json(msg);
        }).catch(e => next(e));

}

function addTimeJob(req, res, next) {
    let post = req.body;
    post.jobasign_id = req.params.id;
    db.daily_jobswork.create(post, {
            silent: true,
            fields: ['updated_at', 'created_at', 'jobasign_id', 'contractor_id', 'crew_count', 'crew_count', 'active']
        })
        .then(() => {
            let msg = {
                status: true,
                msg: message.timeAdded
            }
            res.json(msg);
        }).catch(e => next(e));

}
/**
 * view last entry of job number
 * 
 */
function lastNum(req, res, next) {
    db.jobs.find({
        order: [
            ['number', 'DESC']
        ],
        attributes: ['number'],
        raw: true
    }).then((number) => {
        if (number === null) {
            res.json({
                number: 1000
            });
        } else {
            res.json(number);
        }
    });
}

function deleteJobNote(req, res, next) {
    const id = req.params.jobId;
    // delete from backend_users table
    db.job_notes.destroy({
        where: {
            id: id
        }
        //force: true
    }).then(success => {
        let msg = {
            status: true,
            msg: message.noteDeleted
        }
        res.json(msg);
    }).catch(e => next(e));
}

function filterJob(req, res, next) {
    let body = {};
    db.job_type.find({
        where: {
            id: req.body.jobtype
        },
        raw: true
    }).then((jobtype) => {
        if (jobtype === null) {
            body = {};
        } else {
            body.type = jobtype.title
        }
        db.jobs.findAll({
            where: body,
            raw: true
        }).then((jobs) => {
            if (jobs === null) {
                const msg = {
                    status: false,
                    msg: message.dataNotExist
                };
                res.json(msg);
            } else {
                let result = [];
                async.each(jobs, (job, callback) => {
                    let row = {};
                    row = job;
                    db.office.find({
                        where: {
                            id: job.office_id
                        },
                        attributes: ['name'],
                        raw: true
                    }).then((office) => {
                        if (office !== null) {
                            row.office_name = office.name;
                        }
                        body = {};
                        body.job_id = job.id;
                        if (req.body.contractor_id !== undefined) {
                            body.user_id = req.body.contractor_id;
                        }
                        db.job_assign.find({
                            where: body,
                            raw: true
                        }).then((job_assign) => {
                            if (job_assign === null) {
                                callback();
                            } else {
                                if (job_assign.user_id === null) {
                                    result.push(row);
                                    callback();
                                } else {
                                    db.users.find({
                                        where: {
                                            id: job_assign.user_id
                                        },
                                        attributes: [
                                            ['name', 'contractor_name'], 'last_name'
                                        ],
                                        raw: true
                                    }).then((user) => {
                                        row.contractor_name = user;
                                        result.push(row);
                                        callback();
                                    }).catch(e => next(e));
                                }
                            }
                        }).catch(e => next(e));

                    }).catch(e => next(e));
                }, (err) => {
                    if (err) {
                        console.log('Error grabbing data');
                    } else {

                        res.json(result);
                        console.log('Finished processing all data');
                    }
                });
            }
        }).catch(e => next(e));

    }).catch(e => next(e));

}
/**
 * view current jobs
 * 
 */

function currentJobs(req, res, next) {
    db.job_assign.findAll({
        where: {
            status: 0
        },
        raw: true
    }).then((jobassigns) => {
        if (jobassigns == null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(jobassigns, (jobassign, callback) => {
                let row = {};
                db.jobs.find({
                    where: {
                        id: jobassign.job_id
                    },
                    raw: true
                }).then((job) => {
                    row = _.extend(jobassign, {
                        jobasign_id: jobassign.id
                    }, job);
                    db.daily_jobswork.find({
                        where: {
                            jobasign_id: row.jobasign_id
                        },
                        order: [
                            ['created_at', 'DESC']
                        ],
                        raw: true
                    }).then((lastrecord) => {
                        row.lastrecord = lastrecord;
                        result.push(row);
                        callback();
                    }).catch(e => next(e));
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));
}
export default {
    assignJob,
    getallJobs,
    getJob,
    addJob,
    deleteJob,
    updateJob,
    clockIn,
    clockOut,
    notWork,
    viewRecords,
    status,
    activeJobs,
    lastNum,
    filterJob,
    currentJobs,
    addJobNotes,
    getAllJobNotes,
    editJobNotes,
    deleteJobNote,
    editTimeJob,
    addTimeJob,
    jobsType,
    assignedJobTypes,
    contractorJobs
}