import hat from 'hat';
import async from 'async';
import jwt from 'jsonwebtoken'
import db from '../config/db';
import env from '../config/env';
import message from '../function/messages';
import helpme from '../function/helpme';
import _ from 'underscore';
const crypto = require('crypto');

const date = new Date();

/**
 * OFFICER LOGIN
 * 
 */
function signinOfficer(req, res, next) {
    const encryPass = helpme.encrypt(req.body.password);

    db.users.find({
        where: {
            email: req.body.email,
            password: encryPass,
            $not: {
                type: 1
            }
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notAuth
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                id: success.id,
                type: success.type,
                name: success.name + ' ' + success.last_name,
                profileImage: success.profile_image
            };
            msg.token = jwt.sign(msg, env.CRYPTO_SECRET, {
                expiresIn: 60 * 60 * 24 * 30
              })
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * CHANGE PASSWORD HIMSELF
 * 
 */
function changePassword(req, res, next) {
    const pass = helpme.encrypt(req.body.password);

    db.users.update({
        password: pass
    }, {
        where: {
            id: req.body.id,
            type: 2
        },
        raw: true
    }).then((pass) => {
        if (pass == null) {
            const msg = {
                status: false,
                msg: message.notPass
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.upPass
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * verify token
 * 
 */
function verifyToken(req, res, next) {
    db.users.find({
        where: {
            token: req.body.token
        },
        raw: true,
        attributes: ['id']
    }).then((token) => {
        console.log(token);
        if (token == null) {
            const msg = {
                status: false,
                msg: message.authFalse
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}

/**
 * Forget password
 * 
 */
function forgetPass(req, res, next) {
    const email = req.body.email;
    const token = hat();
    db.users.update({
        token: token
    }, {
        where: {
            email: email
        },
        raw: true
    }).then((setToken) => {
        console.log(setToken);
        if (setToken == 0) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            helpme.forgetPassword(token, email, (cb)=>{
                console.log(cb);
                if (cb.status == true){
                  const msg = {
                        status: true,
                        msg: message.forgetPassTrue
                    };
                  res.json(msg);
                }
                else{
                    const msg = {
                        status: false,
                        msg: message.forgetPassFalse
                    }
                    res.json(msg);
                }
            });
        }
    }).catch(e => next(e));
}
/**
 * reset pasword
 * 
 */
function resetPass(req, res, next){
    const uId = req.params.id;
    const pass = helpme.encrypt(req.body.password);
    db.users.update({
        password: pass,
        token: null
    }, {
        where: {
            token: req.body.token
        },
        raw: true
    }).then((pass) => {
        if (pass == null) {
            const msg = {
                status: false,
                msg: message.notPass
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.upPass
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}


function checkPassword(req, res, next) {
    const pass = helpme.encrypt(req.body.oldpassword);
    db.users.find({
        where: {
            password: pass,
            id: req.body.id
        },
        raw: true
    }).then((pass) => {
        if (pass == null) {
            const msg = {
                status: false
            };
            res.json(msg);
        } else {
            const msg = {
                status: true
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * GET OFFICER
 * 
 */
function getOfficer(req, res, next) {
    let result = [];
    db.users.find({
        where: {
            id: req.params.id,
            type: 2
        },
        raw: true
    }).then((officer) => {
        if (officer === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            result.push(officer);
            db.user_meta.findAll({
                where: {
                    user_id: req.params.id
                }
            }).then((details) => {
                result.push(details);
                db.office.find({
                    where: {
                        id: officer.office_id
                    },
                    attributes: [
                        ['name', 'officename']
                    ]
                }).then((officename) => {
                    result.push(officename);
                    res.json(result);
                }).catch(e => next(e));
            }).catch(e => next(e));
        }
    }).catch(e => next(e));

}
/**
 * UPDATE OFFICER
 * 
 */
function updateOfficer(req, res, next) {
    let data = req.body;
    const id = req.params.id;
    db.users.update(data, {
        where: {
            id: req.params.id,
            type: 2
        },
        raw: true
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            let up = helpme.updateMeta(data, id);
            if (up === false) {
                const msg = {
                    status: false,
                    msg: message.notUpdate
                };
                res.json(msg);
            } else {
                const msg = {
                    status: true,
                    msg: message.update
                };
                res.json(msg);

                if (req.body.changePassword === 0) {
                    console.log('password is same');
                } else {
                    helpme.mailUser(req.body.email, req.body.name, pass, (user) => {
                        console.log(user);
                    });
                }
            }
        }
    }).catch(e => next(e));
}


function comment(req, res, next) {
    db.user_meta.update({
        meta_value: req.body.comment
    }, {
        where: {
            user_id: req.body.id,
            meta_key: 'comments'
        },
        raw: true
    }).then((update) => {
        if (update === null) {
            db.user_meta.create({
                meta_key: 'comments',
                meta_value: req.body.comment
            }).then((create) => {
                if (create === null) {
                    const msg = {
                        status: false,
                        msg: message.notUpdate
                    };
                } else {
                    const msg = {
                        status: true,
                        msg: message.update
                    };
                    res.json(msg);
                }
            }).catch(e => next(e));
        } else {
            const msg = {
                status: true,
                msg: message.update
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}

function officeOfficer(req, res, next) {
    db.users.findAll({
        where: {
            office_id: req.params.id,
            type: 2
        }
    }).then((officers) => {
        res.json(officers);
    }).catch(e => next(e));
}

function activeJobs(req, res, next) {
    let result = [];
    db.users.find({
        where: {
            id: req.params.id
        }
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            db.jobs.findAll({
                where: {
                    office_id: user.office_id
                },
                raw: true
            }).then((jobs) => {
                if (jobs === null) {
                    const msg = {
                        status: false,
                        msg: message.dataNotExist
                    };
                    res.json(msg);
                } else {
                    let result = [];

                    async.each(jobs, (job, callback) => {
                        let row = {};
                        row = job;
                        db.office.find({
                            where: {
                                id: job.office_id
                            },
                            attributes: ['name'],
                            raw: true
                        }).then((office) => {
                            row.office_name = office.name;
                            db.job_assign.find({
                                where: {
                                    job_id: job.id,
                                    status: 0
                                },
                                raw: true
                            }).then((job_assign) => {
                                db.users.find({
                                    where: {
                                        id: job_assign.user_id
                                    },
                                    attributes: ['name' , 'last_name',],
                                    raw: true
                                }).then((user) => {
                                    row.contractor_name = user.name + ' '+ user.last_name;
                                    result.push(row);
                                    callback();
                                }).catch(e => next(e));
                            }).catch(e => next(e));

                        }).catch(e => next(e));
                    }, (err) => {
                        if (err) {
                            console.log('Error grabbing data');
                        } else {
                            res.json(result);
                            console.log('Finished processing all data');
                        }
                    });
                }
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}
export default {
    signinOfficer,
    getOfficer,
    updateOfficer,
    changePassword,
    checkPassword,
    officeOfficer,
    activeJobs,
    forgetPass,
    verifyToken,
    resetPass
}