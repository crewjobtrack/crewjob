import hat from 'hat';
import _ from 'underscore';
import async from 'async';
import fs from 'fs';
import path from 'path';
import multer from 'multer';
import jwt from 'jsonwebtoken'
import db from '../config/db';
import env from '../config/env';
import message from '../function/messages';
import helpme from '../function/helpme';

const crypto = require('crypto');
const date = new Date();
/**
 * Webmaster login
 * 
 */
function signinMaster(req, res, next) {
    const encryPass = helpme.encrypt(req.body.password);

    db.users.find({
        where: {
            email: req.body.email,
            password: encryPass,
            type: 1
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notAuth
            };
            res.json(msg);
        } else {
            let msg = {
                status: true,
                id: success.id,
                profileImage: success.profile_image,
                type: success.type
            };
            msg.token = jwt.sign(msg, env.CRYPTO_SECRET, {
                expiresIn: 60 * 60 * 24 * 30
              })
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * Add office.
 * 
 */
function addOffice(req, res, next) {
    const office = req.body;
    db.office.create(office).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notCreate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * Get list of all offices.
 * @returns {office[]}
 */
function getallOffice(req, res, next) {

    db.office.findAll({

        raw: true
    }).then((offices) => {
        if (offices === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(offices);
        }
    }).catch(e => next(e));
}


/**
 * Get list of all jobTypes.
 */

function getJobTypes(req, res, next) {
    db.job_type.findAll({
            raw: true
        })
        .then((JobTypes) => {
            res.json(JobTypes);
        }).catch(e => next(e));
}

/**
 * Add jobType
 */

function addJobType(req, res, next) {
    db.job_type.create(req.body)
        .then((jobType) => {
            let msg = {
                status: true,
                msg: message.insert
            };
            res.json(msg);
        }).catch(e => next(e));
}

/**
 * Edit jobType
 */

function editJobType(req, res, next) {
    const id = req.params.id;
    db.job_type.update(req.body, {
            where: {
                id: id
            }
        })
        .then((jobType) => {
            let msg = {
                status: true,
                msg: message.update
            };
            res.json(msg);
        }).catch(e => next(e));
}


/**
 * Delete jobType
 */

function deleteJobType(req, res, next) {
    const id = req.params.id;
    db.job_type.destroy({
        where: {
            id: id
        },
        force: true
    }).then((success) => {
        const msg = {
            status: true,
            msg: message.delete
        };
        res.json(msg);
    }).catch(e => next(e));
}

/**
 * Delete Office 
 */
function getOffice(req, res, next) {
    db.office.find({
        where: {
            id: req.params.id
        },
        raw: true
    }).then((office) => {
        if (office === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(office);
        }
    }).catch(e => next(e));
}
/**
 * Update office details
 */
function updateOffice(req, res, next) {
    const office = req.body;

    db.office.update(office, {
        where: {
            id: req.params.id
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * Delete Office 
 */
function deleteOffice(req, res, next) {
    db.office.destroy({
        where: {
            id: req.params.id
        },
        raw: true
    }).then((success) => {
        if (success === 0) {
            const msg = {
                status: false,
                msg: message.notDelete
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                message: message.delete
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * USER CREATION (ADMIN,CONTRACTER,STAFF,OFFICER)
 * 
 */
function createUser(req, res, next) {
    let moredetails = req.body.moredetails;
    const user = {
        email: req.body.email,
        password: helpme.encrypt(req.body.password),
        phone: req.body.phone,
        name: req.body.name,
        last_name: req.body.lname,
        type: req.body.type,
        status: req.body.status || 0,
        office_id: req.body.office_id || null,
    }
    db.users.create(user).then((success) => {
        if (success == null) {
            const msg = {
                status: false,
                msg: message.notCreate
            };
            res.json(msg);
        } else {
            let email = user.email;
            let name = user.name;
            let pass = helpme.decrypt(user.password);
            let uMeta = [];
            async.each(moredetails, (details) => {
                uMeta.push(_.extend(details, {
                    user_id: success.id
                }));
            });
            db.user_meta.bulkCreate(uMeta).then((details) => {
                if (details === null) {
                    helpme.mailUser(email, name, pass, (msg) => {
                        console.log(msg);
                    });
                    const msg = {
                        status: false,
                        msg: message.notCreate
                    };
                    res.json(msg);
                } else {
                    helpme.mailUser(email, name, pass, (msg) => {
                        console.log(msg);
                    });
                    const msg = {
                        status: true,
                        msg: message.userAdded
                    };
                    res.json(msg);
                }
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}
/**
 * VIEWING ALL OFFICERS
 * 
 */
function getallOfficer(req, res, next) {
    db.users.findAll({
        where: {
            type: 2
        },
        raw: true
    }).then((officers) => {
        if (officers === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(officers, (officer, callback) => {
                let row = {};
                row = officer;
                db.office.find({
                    where: {
                        id: officer.office_id
                    },
                    attributes: ['name'],
                    raw: true
                }).then((office) => {
                    if (office === null) {
                        row.office_name = null;
                        result.push(row);
                        callback();
                    } else {
                        row.office_name = office.name;
                        result.push(row);
                        callback();
                    }
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));
}
/**
 * VIEWING A OFFICER
 * 
 */
function getOfficer(req, res, next) {
    db.users.find({
        where: {
            id: req.params.id,
            type: 2
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(success);
        }
    }).catch(e => next(e));
}
/**
 * DELETING OFFICER
 * 
 */
function deleteOfficer(req, res, next) {
    db.users.destroy({
        where: {
            id: req.params.id,
            type: 2
        },
        raw: true
    }).then((contractor) => {
        if (contractor === 0) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            db.user_meta.destroy({
                where: {
                    user_id: req.params.id
                }
            }).then((details) => {
                const msg = {
                    status: true,
                    msg: message.delete
                };
                res.json(msg);
            }).catch(e => next(e));
        }
    }).catch(e => next(e));
}


/**
 * VIEWING ALL USERS
 * 
 */
function getallUser(req, res, next) {
    db.users.findAll({

        raw: true
    }).then((users) => {
        if (users === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(users)
        }
    }).catch(e => next(e));
}
/**
 * ADD ADDITIONAL FIELDS RELATED TO USER IF REQUIRED
 * 
 */
function moreUserDetail(req, res, next) {

    db.user_meta.bulkCreate(req.body).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notCreate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * VIEWING A USER
 * 
 */
function getUser(req, res, next) {
    db.users.find({
        where: {
            id: req.params.id
        },
        raw: true
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(user)
        }
    }).catch(e => next(e));
}
/**
 * UPDATE A USER
 * 
 */
function updateUser(req, res, next) {
    db.users.update(req.body,{
        where: {
            id: req.params.id
        },
        raw: true
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.update
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 *Assigned contractor
 * 
 */
function assignedContractor(req, res, next) {
    db.job_assign.findAll({
        where: {
            job_id: req.params.jobId,
            status: 0
        },
        attributes: ['user_id'],
        raw: true
    }).then((userIds) => {
        let result = [];
        async.each(userIds, (users, callback) => {
            db.users.find({
                where: {
                    id: users.user_id
                },
                attributes: ['name', 'id', 'last_name'],
                raw: true
            }).then((userDetail) => {
                result.push(_.extend(userDetail, {
                    status: 1
                }));
                callback();
            }).catch(e => next(e));
        }, (err) => {
            if (err) {
                console.log('Error grabbing data');
            } else {
                db.users.findAll({
                    where: {
                        type: 3
                    },
                    attributes: ['name', 'id', 'last_name'],
                    raw: true
                }).then((success) => {
                    let resultArray = _.union(result, success)
                    let uniqueLists = _.uniq(resultArray, function (item, key, id) {
                        return item.id
                    });
                    res.json(uniqueLists);
                    console.log('Finished processing all data');
                }).catch(e => next(e));
            }
        });
    }).catch(e => next(e));
}
/**
 * VIEWING ALL CONTRACTOR
 * 
 */
function getallContractor(req, res, next) {
    db.users.findAll({
        where: {
            type: 3
        },
        raw: true
    }).then((success) => {
        let result = [];
        async.each(success, (users, callback) => {

            let user = {};
            db.daily_jobswork.find({
                where: {
                    contractor_id: users.id
                },
                attributes: ['jobasign_id', 'active'],
                order: [
                    ['active', 'DESC']
                ],
                raw: true
            }).then(daily => {
                if (daily == null) {
                    user = _.extend(users, daily);
                    result.push(user);
                    callback();
                } else {
                    user = _.extend(users, daily);
                    db.job_assign.find({
                        where: {
                            id: user.jobasign_id
                        },
                        attributes: ['job_id'],
                        raw: true
                    }).then(jobId => {
                        if (daily == null) {
                            result.push(user);
                            callback();
                        } else {
                            db.jobs.find({
                                where: {
                                    id: jobId.job_id
                                },
                                attributes: ['location', ['id', 'job_id']],
                                raw: true
                            }).then(loc => {
                                user = _.extend(user, loc);

                                result.push(user);
                                callback();
                            }).catch(e => next(e));
                        }
                    }).catch(e => next(e));
                }
            }).catch(e => next(e));
        }, (err) => {
            if (err) {
                console.log('Error grabbing data');
            } else {
                res.json(result);
                console.log('Finished processing all data');
            }
        });
    }).catch(e => next(e));
}
/**
 * VIEWING  CONTRACTOR DETAILS
 * 
 */
function getContractor(req, res, next) {
    let result = [];
    db.users.find({
        where: {
            id: req.body.id,
            type: 3
        },
        raw: true
    }).then((contractor) => {
        if (contractor === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            result.push(contractor);
            db.user_meta.findAll({
                where: {
                    user_id: req.body.id
                }
            }).then((details) => {
                if (details === null) {
                    res.json(result);
                } else {
                    result.push(details);
                    res.json(result);
                }
            }).catch(e => next(e));
        }
    }).catch(e => next(e));

}
/**
 * DELETE CONTRACTOR
 * 
 */
function deleteContractor(req, res, next) {
    db.job_assign.find({
        where: {
            user_id: req.params.id
        },
        raw: true,
        attributes: ['user_id']
    }).then((jobAssign) => {
        if (jobAssign === null) {
            db.users.destroy({
                where: {
                    id: req.params.id,
                    type: 3
                },
                raw: true
            }).then((contractor) => {
                if (contractor === 0) {
                    const msg = {
                        status: false,
                        msg: message.dataNotExist
                    };
                    res.json(msg);
                } else {
                    db.user_meta.destroy({
                        where: {
                            user_id: req.params.id
                        }
                    }).then((details) => {
                        const msg = {
                            status: true,
                            msg: message.delete
                        };
                        res.json(msg);
                    }).catch(e => next(e));
                }
            }).catch(e => next(e));
        } else {
            const msg = {
                status: false,
                msg: message.contractorDeleteFalse
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * UPDATE CONTRACTOR
 * 
 */
function updateContractor(req, res, next) {
    let data = req.body;
    const id = req.params.id;
    db.users.update(data, {
        where: {
            id: req.params.id,
            type: 3
        },
        raw: true
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            let up = helpme.updateMeta(data, id);
            if (up === false) {
                const msg = {
                    status: false,
                    msg: message.notUpdate
                };
                res.json(msg);
            } else {
                const msg = {
                    status: true,
                    msg: message.update
                };
                res.json(msg);

                if (req.body.changePassword == 0) {
                    console.log('password is same');
                } else {
                    helpme.mailUser(req.body.email, req.body.name, pass, (user) => {
                        console.log(user);
                    });
                }
            }
        }
    }).catch(e => next(e));
}

/**
 * ADDING NEW PAYMENTS
 * 
 */
function addPayment(req, res, next) {
    db.payments.create(req.body).then((payment) => {
        if (payment === null) {
            const msg = {
                status: false,
                msg: message.notCreate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.insert
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}

/**
 * Add jobc in archive
 * 
 */
function archiveJob(req, res, next) {
    const status = req.body.status;
    const id = req.params.id;
    req.body.updated_at = new Date();
    console.log(req.body);
    db.jobs.update(req.body, {
        where: {
            id: id
        },
        raw: true
    }).then((job) => {
        if (status == 2) {
            const msg = {
                status: true,
                msg: message.archiveJob
            };
            res.json(msg);
            archiveStatus(id, (cb) => {
                console.log(cb);
            });
        } else {
            const msg = {
                status: true,
                msg: message.resumeJob
            }
            res.json(msg);
        }
    }).catch(e => next(e));
}

function archiveStatus(id, cb) {
    console.log('3333333333');
    db.job_assign.findAll({
        where: {
            job_id: id
        },
        raw: true,
        attributes: ['id']
    }).then(jobassign => {
        const ids = _.pluck(jobassign, 'id');
        db.daily_jobswork.update({
            updated_at: new Date()
        }, {
            where: {
                jobasign_id: {
                    $in: ids
                }
            }
        }).then(() => {
            return cb({
                status: true
            });
        }).catch(e => cb(e));
    }).catch(e => cb(e));
}
/**
 * VIEWING ALL PAYMENTS
 * 
 */
function getPayments(req, res, next) {
    db.payments.findAll({

        raw: true
    }).then((payments) => {
        if (payments === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(payments);
        }
    }).catch(e => next(e));
}
/**
 * VIEWING A PAYMENT
 * 
 */
function getPayment(req, res, next) {
    db.payments.find({
        where: {
            id: req.params.id
        },
        raw: true
    }).then((payment) => {
        if (payment === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(payment);
        }
    }).catch(e => next(e));
}

/**
 * UPDATE A PAYMENT
 * 
 */
function updatePayment(req, res, next) {
    db.payments.update(req.body, {
        where: {
            id: req.params.id
        },
        raw: true
    }).then((payment) => {
        if (payment === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.update
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * DELETE A PAYMENT
 * 
 */
function deletePayment(req, res, next) {
    db.payments.destroy({
        where: {
            id: req.params.id
        },
        raw: true
    }).then((payment) => {
        if (contractor === 0) {
            const msg = {
                status: false,
                msg: message.notDelete
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.delete
            };
        }
    }).catch(e => next(e));
}
/**
 * EMAIL EXISTS
 * 
 */
function ifExists(req, res, next) {
    db.users.find({
        where: {
            email: req.body.email
        },
        raw: true
    }).then((exits) => {
        if (exits === null) {
            const msg = {
                status: true,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            const msg = {
                status: false,
                msg: message.exits
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * GET ALL ADMINS
 * 
 */
function getallAdmins(req, res, next) {
    const id = req.params.id;
    db.users.findAll({
        where: {
            type: 1,
            id: {
                $not: id
            }
        },
        raw: true
    }).then((admins) => {
        if (admins === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(admins);
        }
    }).catch(e => next(e));
}
/**
 * GET A ADMIN
 * 
 */
function getAdmin(req, res, next) {
    db.users.find({
        where: {
            id: req.params.id,
            type: 1
        },
        raw: true
    }).then((user) => {
        if (user === null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            res.json(user);
        }
    }).catch(e => next(e));
}
/**
 * DELETE ADMIN
 * 
 */
function delAdmin(req, res, next) {
    db.users.destroy({
        where: {
            id: req.params.id,
            type: 1
        },
        raw: true
    }).then((admins) => {
        if (admins === 0) {
            const msg = {
                status: false,
                msg: message.notDelete
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.delete
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}

/**
 * confirm password of frontend user
 */

function confirmPassword(req, res, next) {
    const password = req.body.password;
    const encryPass = helpme.encrypt(password);
    const id = req.params.id;
    db.users.find({
            where: {
                id: id,
                password: encryPass
            },
            attributes: ['id'],
            raw: true
        })
        .then((success) => {
            if (success === null) {
                const msg = {
                    status: false,
                    msg: message.passwordFalse
                };
                res.json(msg);
            } else {
                const msg = {
                    status: true,
                    msg: message.passwordTrue
                };
                res.json(msg);
            }
        }).catch(e => next(e));
}



/**
 * change password of user
 */

function changePassword(req, res, next) {
    const password = req.body.password;
    const encryPass = helpme.encrypt(password);
    const id = req.params.id;
    const setPass = {
        password: encryPass
    };
    db.users.update(setPass, {
            where: {
                id: id
            },
            attributes: ['id'],
            raw: true
        })
        .then((success) => {
            const msg = {
                status: true,
                msg: message.setPassword
            };
            res.json(msg);
        }).catch(e => next(e));
}

/**
 * UPDATE ADMIN
 * 
 */
function updateAdmin(req, res, next) {
    let data = req.body;
    db.users.update(data, {
        where: {
            id: req.params.id,
            type: 1
        },
        raw: true
    }).then((admins) => {
        if (admins === null) {
            const msg = {
                status: false,
                msg: message.notUpdate
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.update
            };
            res.json(msg);

            if (req.body.changePassword == 0) {
                console.log('password is same');
            } else {
                helpme.mailUser(req.body.email, req.body.name, pass, (user) => {
                    console.log(user);
                });
            }
        }
    }).catch(e => next(e));
}

/**
 * Get all leads.
 */

function getLeads(req, res, next) {
    db.leads.findAll({
            raw: true
        })
        .then((leads) => {
            if (leads.length === 0) {
                const msg = {
                    status: false,
                    msg: message.dataNotExist
                };
                res.json(msg);
            } else {
                let resultArray = [];
                async.each(leads, function (lead, callback) {
                    db.users.find({
                        where: {
                            id: lead.officer_id
                        },
                        raw: true,
                        attributes: ['name', 'last_name']
                    }).then(officer => {
                        let officerName = (officer == null) ?
                            null :
                            officer.name + ' ' + officer.last_name;
                        let row = {};
                        row = lead;
                        row.officerName = officerName;
                        resultArray.push(row);
                        callback();
                    }).catch(e => next(e));
                }, function (err) {
                    if (err) {
                        console.log("Error grabbing data");
                    } else {
                        res.json(resultArray);
                        console.log("Finished processing all data");
                    }
                });
            }
        }).catch(e => next(e));
}


/**
 * Get lead.
 */

function getLead(req, res, next) {
    db.leads.find({
            where: {
                id: req.params.id
            },
            raw: true
        })
        .then((lead) => {
            if (lead == null) {
                const msg = {
                    status: false,
                    msg: message.dataNotExist
                };
                res.json(msg);
            } else {
                let resultArray = [];
                db.users.find({
                    where: {
                        id: lead.officer_id
                    },
                    raw: true,
                    attributes: ['name', 'last_name']
                }).then(officer => {
                    let officerName = (officer == null) ?
                        null :
                        officer.name + ' ' + officer.last_name;
                    let row = {};
                    db.leads_notes.findAll({
                        where: {
                            lead_id: lead.id
                        },
                        raw: true,
                        attributes: ['note', 'user_id', 'created_at', 'id', 'lead_id']
                    }).then(notes => {
                        let resultArray = [];
                        if (notes.length == 0) {

                            row = lead;
                            row.officerName = officerName;
                            res.json(row);
                        } else {
                            async.each(notes, function (note, callback) {
                                db.users.find({
                                    where: {
                                        id: note.user_id
                                    },
                                    raw: true,
                                    attributes: ['name', 'last_name']
                                }).then(noteBy => {
                                    console.log(noteBy);
                                    let noteby = {};
                                    if (noteBy == null) {
                                        noteby = null;
                                    } else {
                                        noteby = noteBy.name + ' ' + noteBy.last_name;
                                    }
                                    row = note;
                                    row.name = noteby;
                                    resultArray.push(row);
                                    callback();
                                }).catch(e => next(e));
                            }, function (err) {
                                if (err) {
                                    console.log("Error grabbing data");
                                } else {
                                    let rows = {};
                                    rows = lead;
                                    rows.officerName = officerName;
                                    rows.notes = resultArray;
                                    res.json(rows);
                                    console.log("Finished processing all data");
                                }
                            });
                        }
                    }).catch(e => next(e));
                }).catch(e => next(e));
            }
        }).catch(e => next(e));
}
/**
 * Add lead.
 */

function addLead(req, res, next) {
    const lead = req.body;
    const name = req.body.name;
    const officerId = req.body.officer_id;
    const address = req.body.address;
    const contactNo = req.body.contact_no;
    const lat = req.body.lat;
    const lng = req.body.lng;
    db.leads.create(lead)
        .then(lead => {
            const post = {
                lead_id: lead.id,
                note: req.body.note
            }
            db.leads_notes.create(post).then(lead => {
                const msg = {
                    status: true,
                    msg: message.leadAdded
                };
                res.json(msg);
                helpme.lead(name, address, contactNo, officerId,lat, lng, (cb) => {
                    console.log(cb);
                });
            }).catch(e => next(e));
        }).catch(e => next(e));
}

/**
 * Edit lead.
 */

function editLead(req, res, next) {
    db.leads.update(req.body, {
            where: {
                id: req.body.id
            },
            raw: true
        })
        .then((success) => {
            const msg = {
                status: true,
                msg: message.leadEdited
            };
            res.json(msg);
            if (req.body.status == true) {
                const name = req.body.name;
                const officerId = req.body.officer_id;
                helpme.lead(name, officerId, (cb) => {
                    console.log(cb);
                });
            } else {
                console.log('Officer not changed');
            }

        }).catch(e => next(e));
}

function leadStatus(req, res, next) {
    db.leads.update(req.body, {
            where: {
                id: req.params.id
            },
            raw: true
        })
        .then((success) => {
            const msg = {
                status: true
            };
            res.json(msg);
        }).catch(e => next(e));
}



function updateUserProfileImg(req, res, next) {
    let resultArray = [];
    let storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, '/var/www/html/backend/uploads/');
        },
        filename: function (req, file, cb) {
            let fileName = {};
            var datetimestamp = Date.now();
            fileName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
            resultArray.push(fileName);
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
        }
    });
    let upload = multer({ //multer settings
        storage: storage
    }).array('userPhoto', 10);
    upload(req, res, function (err) {
        if (err) {
            res.json(err);
            return;
        }
        res.json(resultArray);
        console.log(resultArray);
    });
  }
  

/**
 * Edit note.
 */

function editNote(req, res, next) {
    db.leads_notes.update(req.body, {
            where: {
                id: req.body.id
            },
            raw: true
        })
        .then((success) => {
            const msg = {
                status: true,
                msg: message.noteEdited
            };
            res.json(msg);
        }).catch(e => next(e));
}


function loginRequired(req, res, next) {
    if (req.user) {
      next();
    } else {
      return res.status(401).json({
        message: 'Unauthorized user!'
      });
    }
  };

/**
 * Add note.
 */

function addNote(req, res, next) {
    db.leads_notes.create(req.body).then(note => {
        const msg = {
            status: true,
            msg: message.noteAddedLead
        };
        res.json(msg);
    }).catch(e => next(e));
}



// test commit
function test(req, res, next) {
    const data = req.body;
    let check = [];
    for (var key in data) {
        let metakey = key;
        let metavalue = data[key];
        console.log('Meta key here');
        console.log(metakey);
        console.log('Meta value here');
        console.log(metavalue);
        db.user_meta.update({
            meta_value: metavalue
        }, {
            where: {
                meta_key: metakey
            }
        }).then((success) => {
            check.push(success);
        }).catch(e => next(e));
    }
    if (check === null) {
        res.json('false');
    } else {
        res.json('true');
    }

}

export default {
    signinMaster,
    getallOffice,
    addOffice,
    updateOffice,
    deleteOffice,
    getallOfficer,
    getOfficer,
    deleteOfficer,
    createUser,
    getallUser,
    moreUserDetail,
    getOffice,
    getallContractor,
    getContractor,
    updateContractor,
    deleteContractor,
    getUser,
    updateUser,
    getPayments,
    getPayment,
    addPayment,
    updatePayment,
    deletePayment,
    test,
    getJobTypes,
    addJobType,
    editJobType,
    deleteJobType,
    ifExists,
    getallAdmins,
    getAdmin,
    delAdmin,
    getAdmin,
    updateAdmin,
    confirmPassword,
    changePassword,
    assignedContractor,
    archiveJob,
    getLeads,
    addLead,
    editLead,
    addNote,
    getLead,
    editNote,
    leadStatus,
    updateUserProfileImg,
    loginRequired
};