import hat from 'hat';
import async from 'async';
import jwt from 'jsonwebtoken'
import db from '../config/db';
import env from '../config/env';
import message from '../function/messages';
import helpme from '../function/helpme';
import _ from 'underscore';
const crypto = require('crypto');

const date = new Date();
/**
 * Contracter login
 * 
 */
function signinContracter(req, res, next) {
    const encryPass = helpme.encrypt(req.body.password);

    db.users.find({
        where: {
            email: req.body.email,
            password: encryPass,
            type: 3
        },
        raw: true
    }).then((success) => {
        if (success === null) {
            const msg = {
                status: false,
                msg: message.notAuth
            };
            res.json(msg);
        } else {
            let msg = {
                status: true,
                id: success.id,
                type: success.type,
                name: success.name
            };
            msg.token = jwt.sign(msg, env.CRYPTO_SECRET, {
                expiresIn: 60 * 60 * 24 * 30
              })
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * CHANGE PASSWORD HIMSELF
 * 
 */
function changePassword(req, res, next) {
    const pass = helpme.encrypt(req.body.password);

    db.users.update({
        password: pass
    }, {
        where: {
            id: req.body.id,
            type: 3
        },
        raw: true
    }).then((pass) => {
        if (pass == null) {
            const msg = {
                status: false,
                msg: message.notPass
            };
            res.json(msg);
        } else {
            const msg = {
                status: true,
                msg: message.upPass
            };
            res.json(msg);
        }
    }).catch(e => next(e));
}
/**
 * COMPLETED JOBS UNDER CONTRACTER 
 * 
 */
function mycomJobs(req, res, next) {
    db.job_assign.findAll({
        where: {
            user_id: req.params.id,
            status: 1
        },
        raw: true
    }).then((jobassigns) => {
        if (jobassigns == null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(jobassigns, (jobassign, callback) => {
                db.jobs.find({
                    where: {
                        id: jobassign.job_id
                    },
                    raw: true
                }).then((job) => {
                    result.push(_.extend(jobassign, job));
                    callback();
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));
}
/**
 * PENDING JOBS UNDER CONTRACTER 
 * 
 */
function mypenJobs(req, res, next) {
    db.job_assign.findAll({
        where: {
            user_id: req.params.id,
            status: 0
        },
        raw: true
    }).then((jobassigns) => {
        if (jobassigns == null) {
            const msg = {
                status: false,
                msg: message.dataNotExist
            };
            res.json(msg);
        } else {
            let result = [];
            async.each(jobassigns, (jobassign, callback) => {
                let row = {};
                db.jobs.find({
                    where: {
                        id: jobassign.job_id,
                        status: {
                            $in: [0, 1]
                        }
                    },
                    raw: true
                }).then((job) => {
                    if (job == null) {
                        callback();
                    } else {
                        row = _.extend(jobassign, {
                            jobasign_id: jobassign.id
                        }, job);
                        db.daily_jobswork.find({
                            where: {
                                jobasign_id: row.jobasign_id
                            },
                            order: [
                                ['created_at', 'DESC']
                            ],
                            raw: true
                        }).then((lastrecord) => {
                            row.lastrecord = lastrecord;
                            result.push(row);
                            callback();
                        }).catch(e => next(e));
                    }
                }).catch(e => next(e));
            }, (err) => {
                if (err) {
                    console.log('Error grabbing data');
                } else {
                    res.json(result);
                    console.log('Finished processing all data');
                }
            });
        }
    }).catch(e => next(e));
}
export default {
    signinContracter,
    changePassword,
    mycomJobs,
    mypenJobs
}