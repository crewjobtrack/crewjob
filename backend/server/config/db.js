const Sequelize = require('sequelize');
const env = require('./env');
const sequelize = new Sequelize(env.DATABASE_NAME, env.DATABASE_USERNAME, env.DATABASE_PASSWORD, {
    host: env.DATABASE_HOST,
    port: env.DATABASE_PORT,
    dialect: env.DATABASE_DIALECT,
    define: {
        underscored: true
    }
});
//
// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
//Models/tables
db.users = require('../models/users.js')(sequelize, Sequelize);
db.user_meta = require('../models/user_meta.js')(sequelize, Sequelize);
db.office = require('../models/office.js')(sequelize, Sequelize);
db.jobs = require('../models/jobs.js')(sequelize, Sequelize);
db.job_assign = require('../models/job_assign.js')(sequelize, Sequelize);
db.payments = require('../models/payments.js')(sequelize, Sequelize);
db.daily_jobswork = require('../models/daily_jobswork.js')(sequelize, Sequelize);
db.job_type = require('../models/job_type.js')(sequelize, Sequelize);
db.job_notes = require('../models/job_notes')(sequelize, Sequelize);
db.leads = require('../models/leads')(sequelize, Sequelize);
db.leads_notes = require('../models/leads_notes')(sequelize, Sequelize);
module.exports = db;