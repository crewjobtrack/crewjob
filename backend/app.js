import app from './server/config/express';
import db from './server/config/db.js';
import config from './server/config/env';

const debug = require('debug')('CrewJob:index');
const port = process.env.PORT || config.PORT;
const env = process.NODE_ENV || config.NODE_ENV;

// connecting the database
// listen server on port 
db.sequelize.sync().then((db) => {
  console.log('Database connected');
  const server = app.listen(port, () => {
    console.log('Server listning on port ' + port);
  });
});