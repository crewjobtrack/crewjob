###PROJECT: Crew Jobtrack

To get started:
 1. make sure you're using Node > 4.2, yarn and gulp install on your system.
 2. `yarn install` <!-- install all dependecies of package.json file -->
 3. `yarn build` <!-- build a dist folder -->
 4. `yarn start` <!-- start gulp serve and run server on specific port-->