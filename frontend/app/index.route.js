app.config(function($routeProvider) {
$routeProvider
.when("/", {
templateUrl : "partials/officer/login.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/addlead", {
templateUrl : "partials/leadtracker.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/webmaster", {
templateUrl : "partials/webmaster/login.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-offices", {
templateUrl : "partials/webmaster/office.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addoffice", {
templateUrl : "partials/webmaster/addoffice.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editlead/:id", {
templateUrl : "partials/webmaster/editlead.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewlead/:id", {
templateUrl : "partials/webmaster/viewlead.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editoffice/:id", {
templateUrl : "partials/webmaster/editoffice.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewoffice/:id", {
templateUrl : "partials/webmaster/viewoffice.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-officers", {
templateUrl : "partials/webmaster/officers.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addofficers", {
templateUrl : "partials/webmaster/addofficers.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewofficers/:id", {
templateUrl : "partials/webmaster/viewofficers.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editofficers/:id", {
templateUrl : "partials/webmaster/editofficers.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-contractors", {
templateUrl : "partials/webmaster/contractors.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addcontractors", {
templateUrl : "partials/webmaster/addcontractors.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewcontractors/:id", {
templateUrl : "partials/webmaster/viewcontractors.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editcontractors/:id", {
templateUrl : "partials/webmaster/editcontractors.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-admins", {
templateUrl : "partials/webmaster/admins.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-archivejob", {
templateUrl : "partials/webmaster/archivejob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addadmin", {
templateUrl : "partials/webmaster/addadmin.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewadmin/:id", {
templateUrl : "partials/webmaster/viewadmin.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editadmin/:id", {
templateUrl : "partials/webmaster/editadmin.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-job", {
templateUrl : "partials/webmaster/jobs.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addjob", {
templateUrl : "partials/webmaster/addjob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editjob/:id", {
templateUrl : "partials/webmaster/editjob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewjob/:id", {
templateUrl : "partials/webmaster/viewjob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-activejob", {
templateUrl : "partials/webmaster/activejob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewactivejob/:id", {
templateUrl : "partials/webmaster/viewactivejob.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-jobtype", {
templateUrl : "partials/webmaster/jobtype.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addjobtype", {
templateUrl : "partials/webmaster/addjobtype.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-editjobtype/:id", {
templateUrl : "partials/webmaster/editjobtype.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-addlead", {
templateUrl : "partials/webmaster/leadtracker.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-leads", {
templateUrl : "partials/webmaster/leads.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-changepassword", {
templateUrl : "partials/webmaster/changepassword.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-viewProfile", {
templateUrl : "partials/webmaster/viewwebmasters.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-updateprofile", {
templateUrl : "partials/webmaster/update-profile.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/webmaster-alljobs", {
templateUrl : "partials/webmaster/alljobs.html",
controller: 'webmaster',
title: 'Job Tracking'
})
.when("/officer-addlead", {
templateUrl : "partials/officer/leadtracker.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-leads", {
templateUrl : "partials/officer/leads.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-editlead/:id", {
templateUrl : "partials/officer/editlead.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-viewlead/:id", {
templateUrl : "partials/officer/viewlead.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-jobs", {
templateUrl : "partials/officer/job.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-alljobs", {
templateUrl : "partials/officer/alljobs.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-addjob", {
templateUrl : "partials/officer/addjob.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-viewjobs/:id", {
templateUrl : "partials/officer/viewjob.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-editjobs/:id", {
templateUrl : "partials/officer/editjob.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-viewprofile", {
templateUrl : "partials/officer/viewprofile.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-updateprofile", {
templateUrl : "partials/officer/updateprofile.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-changepassword", {
templateUrl : "partials/officer/changepassword.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-contractors", {
templateUrl : "partials/officer/contractors.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-viewcontractors/:id", {
templateUrl : "partials/officer/viewcontractors.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-addcontractors", {
templateUrl : "partials/officer/addcontractors.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-editcontractors/:id", {
templateUrl : "partials/officer/editcontractors.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/officer-activejob", {
templateUrl : "partials/officer/activejob.html",
controller: 'officer',
title: 'Job Tracking'
})
.when("/contractor-jobs", {
templateUrl : "partials/contractor/job.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-completedjobs", {
templateUrl : "partials/contractor/completejob.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-pendingjobs", {
templateUrl : "partials/contractor/pendingjob.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-workingjob", {
templateUrl : "partials/contractor/workingjob.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-viewjobs/:id", {
templateUrl : "partials/contractor/viewjob.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-viewProfile", {
templateUrl : "partials/contractor/viewcontractors.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-alljobhistory", {
templateUrl : "partials/contractor/alljobhistory.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-jobhistory", {
templateUrl : "partials/contractor/jobhistory.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/contractor-editProfile", {
templateUrl : "partials/contractor/editcontractors.html",
controller: 'contractor',
title: 'Job Tracking'
})
.when("/forgotpassword", {
templateUrl : "partials/forgotpassword.html",
controller: 'frontend',
title: 'Job Tracking'
})
.when("/forgotpassword/thankyou", {
templateUrl : "partials/forgotthankyou.html",
controller: 'frontend',
title: 'Job Tracking'
})
.when("/resetpassword", {
templateUrl : "partials/resetpassword.html",
controller: 'frontend',
title: 'Job Tracking'
})
.when("/linkexpthankyou", {
templateUrl : "partials/linkexp.html",
controller: 'frontend',
title: 'Job Tracking'
})
.when("/thankyou", {
templateUrl : "partials/thankyou.html",
controller: 'frontend',
title: 'Job Tracking'
})

});