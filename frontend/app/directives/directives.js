app.directive('pwCheck', [function () {
    return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
    var firstPassword = '#' + attrs.pwCheck;
    elem.add(firstPassword).on('keyup', function () {
    scope.$apply(function () {
    var v = elem.val()===$(firstPassword).val();
    ctrl.$setValidity('pwmatch', v);
    });
    });
    }
    };
}]);

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('imageReader', function ($q) {
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function () {};

            element.bind('change', function (e) {
                var element = e.target;

                $q.all(slice.call(element.files, 0).map(readFile))
                    .then(function (values) {
                    if (element.multiple) ngModel.$setViewValue(values);
                    else ngModel.$setViewValue(values.length ? values[0] : null);
                });

                function readFile(file) {
                     var deferred = $q.defer();

                     var reader = new FileReader();
                     reader.onload = function (e) {
                         deferred.resolve(e.target.result);
                     };
                    // reader.onerror = function (e) {
                    //     deferred.reject(e);
                    // };
                     reader.readAsDataURL(file);

                    return deferred.promise;
                }
            });
        }
    };
});

app.directive('player', ['$sce', function ($sce) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            videos: '='
        },
        link: function (scope, element, attrs) {
            var video = element.find('video');
            element.addClass('player');
            scope.playing = false;
            scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            }
            
            video.on('timeupdate', function (e) {
                scope.$apply(function () {
                    scope.percent = (video[0].currentTime / video[0].duration) * 100;
                });
            });

            scope.frame = function (num) {
                if (video[0].readyState !== 0) {
                    video[0].currentTime += num;
                }
            };

            scope.toggle = function () {
                if (video[0].paused === true) {
                    video[0].play();
                    scope.playing = true;
                } else {
                    video[0].pause();
                    scope.playing = false;
                }
            };
        },
        template: '<video  ng-click="toggle()" preload="none" poster="{{ trustSrc(videos[0].poster) }}">' +
            '<source ng-repeat="item in videos" ng-src="{{ trustSrc(item.src) }}" type="video/{{ item.type }}" />' +
            '<track kind="captions" ng-src="{{ trustSrc(videos[0].captions) }}" srclang="en" label="English" />' +
            '</video>' +
            '<progressbar value="percent" max="100"></progressbar>' +
            '<div class="controls noselect">' +
            '<a ng-click="frame(-0.04)">&lt;</a>' +
            '<a ng-click="toggle()"> <span ng-show="!playing">&#9654;</span><span ng-show="playing">&#9616;&#9616;</span> </a>' +
            '<a ng-click="frame(0.04)">&gt;</a>' +
            '</div>'
    };
}]);


app.directive('checkImage', function ($q) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            attrs.$observe('ngSrc', function (ngSrc) {
                var deferred = $q.defer();
                var image = new Image();
                image.onerror = function () {
                    deferred.resolve(false);
                    element.attr('src', 'images/user-image-with-black-background.png'); // set default image
                };
                image.onload = function () {
                    deferred.resolve(true);
                };
                image.src = ngSrc;
                return deferred.promise;
            });
        }
    };
});


app.filter('range', function() {
  return function(input, min, max) {
    min = min; //Make string input int
    max = max;
    input.push("other");
    for (var i=min; i<max; i++)

      input.push(i);  
    return input;
  };
});

app.filter('capitalize', function() {
  return function(input, scope) {
    if(input.indexOf(' ') !== -1){
      var inputPieces,
          i;

      input = input.toLowerCase();
      inputPieces = input.split(' ');

      for(i = 0; i < inputPieces.length; i++){
        inputPieces[i] = capitalizeString(inputPieces[i]);
      }

      return inputPieces.toString().replace(/,/g, ' ');
    }
    else {
      input = input.toLowerCase();
      return capitalizeString(input);
    }

    function capitalizeString(inputString){
      return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
    }
  }
});

app.filter('mySort', function() {
    return function(input) {
      return input.sort();
    }
  });
/*angular.module('UserValidation', []).directive('validPassword', function () {
return {
require: 'ngModel',
link: function (scope, elm, attrs, ctrl) {
ctrl.$parsers.unshift(function (viewValue, $scope) {
var noMatch = viewValue != scope.signup_form.password.$viewValue
ctrl.$setValidity('noMatch', !noMatch)
})
}
}
});*/

