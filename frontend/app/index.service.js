app.service('userService', function ($http, $rootScope, $location, $window, $rootScope, $timeout) {
	this.adminLogout = function(){
		localStorage.removeItem('aid');
		localStorage.removeItem('type');
		localStorage.removeItem('redirect');
		$rootScope.userLogin = true; 
		$location.path('/webmaster');
        
	};

	/*this.userLogout = function(){
		localStorage.removeItem('id');
		$location.path('/');
	};*/

	this.backofficeLogout = function(){
		
		$location.path('/back-office');
	};
	this.editUsersProfile = function(){
		$location.path('/backoffice_profile');	
	};
	
	
	this.responseMsg = function($scope){
	if(localStorage.getItem("MSG") != null && localStorage.getItem("MSG") != 'null'){
		$rootScope.showSuccessMsg = localStorage.getItem("MSG");
		$rootScope.showSuccess = true;
		localStorage.setItem("MSG",null);
	}else{
		$rootScope.showSuccess = false;
		$rootScope.showSuccessMsg = '';
	} 
	if(localStorage.getItem("ErrMSG") != null && localStorage.getItem("ErrMSG") != 'null'){
		$rootScope.responseErrorMSG = localStorage.getItem("ErrMSG");
		$rootScope.resErrorMsg = true;
		localStorage.setItem("ErrMSG",null);
    }else{
         $rootScope.resErrorMsg = false;
         $rootScope.responseErrorMSG = '';   
        }
	
	$timeout(function () {
	$rootScope.resErrorMsg = false;
	$rootScope.showSuccess = false;
	}, 5000);
	};

});

app.filter('nl2br', function($sce){
      return function(msg,is_xhtml) { 
          var is_xhtml = is_xhtml || true;
          var breakTag = (is_xhtml) ? '<br />' : '<br>';
          var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
          return $sce.trustAsHtml(msg);
      }
  });

app.service('Map', function($q) {
    
    this.init = function() {
        var options = {
            center: new google.maps.LatLng(34.0512519 , -84.7211074),
            zoom: 15,
            mapTypeControl: true  
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options);
        this.places = new google.maps.places.PlacesService(this.map);
    }
    
    this.search = function(str) {
        var d = $q.defer();
        this.places.textSearch({query: str}, function(results, status) {
            if (status == 'OK') {
                d.resolve(results[0]);
            }
            else d.reject(status);
        });
        return d.promise;
    }
    
    this.addMarker = function(res) {
        if(this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }
    
});
