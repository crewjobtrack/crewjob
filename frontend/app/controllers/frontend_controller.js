app.controller('frontend', function($scope,Map,userFactory,$http,$location,$rootScope, DTOptionsBuilder, DTColumnBuilder, $timeout,JOBTRACKING_CONSTANT,userService,$routeParams) {

 
    $scope.forgotPassword = function() {
        $location.path('/forgotpassword');
    };
   var token = $location.absUrl();
    var index = token.search("resetpassword");
    if (index != -1) {
        sURLVariables1 = token.split('token=');
        var token1 = sURLVariables1[1];
        var Ttype = "";
        if (token1 != null || token1 != undefined) {
            apiURL = JOBTRACKING_CONSTANT.URL + "/officer/verifyToken" ;
            var data = {
              token :token1 
            };
            $http({
                method: 'post',
                data:data,
                url: apiURL,
                headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                if (response.data.status != false) {
                    $location.url('/resetpassword?token='+token1);
                } else {
                    $location.url('/linkexpthankyou');
                }
            });
        }
    } else {
        sURLVariables1 = token.split('token=');
        var token1 = sURLVariables1[1];
    }
   
     $scope.backpage = function() {
      window.history.back();
    }
    $scope.forgotPass = function() {
        apiURL = JOBTRACKING_CONSTANT.URL + "/officer/forgetPassword";
        var data = {
            email: $scope.usr.email,
        };
        $http({
            method: 'Post',
            url: apiURL,
            data: data,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }

        }).then(function(response) {
            if (response.data.status) {
                $location.path('/forgotpassword/thankyou');
            } else {
                $scope.responseMessage = 'Please enter a correct email id!';
            }
        });
    };

     /*-------------reset password-------------*/

  $scope.resetPassword = function(isresetPassword) {
        if (isresetPassword) {
            apiURL = JOBTRACKING_CONSTANT.URL + "/officer/forgetPassword";
            var data = {
                password: $scope.password,
                token:token1,
            };
            $http({
                method: 'PUT',
                url: apiURL,
                data: data,
                headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                if (response.data.status) {
                    //alert('password change successfully!');
                    $location.url('/thankyou');
                } else {
                    $scope.resetmsg = response.data.msg;
                }
            });
        };
    };

});