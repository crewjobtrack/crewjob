app.controller('officer', function ($scope, Map, userFactory, $http, $location, $rootScope, DTOptionsBuilder, DTColumnBuilder, $timeout, JOBTRACKING_CONSTANT, userService, $routeParams) {
    $scope.finalstatus = true;
    if (jQuery(window).width() < 768) {
        $scope.collapseClass = 'collapse';
        /*----------webmaster login factory call---------*/
    } else {
        $scope.collapseClass = '';
    }
    if (($location.$$path != '/addlead') && (localStorage.getItem('type') != 2 && (localStorage.getItem('id') == '' || localStorage.getItem('id') == null || localStorage.getItem('id') == undefined))) {
        $location.path('/');
    }
    $scope.login_user_name = localStorage.getItem("login_user_name");
    $scope.Loginid = localStorage.getItem('id');
    $scope.loginType = localStorage.getItem('type');
    $scope.officerLogin = function () {
        userFactory.frontendLogin($scope);
    };
    $scope.backpage = function () {
        window.history.back();
    }
    /*-------------multiple locations point out----------*/
    var locations = $rootScope.locations;
    var map;
    var markers = [];

    function init() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 15,
            mapTypeControl: true,
            center: new google.maps.LatLng(34.051252, -84.721107),
        });
        var num_markers = locations.length;
        console.log(num_markers);
        for (var i = 0; i < num_markers; i++) {
            latlngset = new google.maps.LatLng(locations[i][2], locations[i][3]);
            var marker = new google.maps.Marker({
                map: map,
                title: locations[i][0],
                position: latlngset
            });
            map.setCenter(marker.getPosition())
            var content = "Job title: " + locations[i][0] + ',</br></h3>' + "Contractor: " + locations[i][4] + ',</br></h3>' + "Address: " + locations[i][1] + "</br>" + "<a href=#/officer-viewjobs/" + locations[i][5] + ">" + "Details</a>"
            var infowindow = new google.maps.InfoWindow()
            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));
        }
    };
    var Joblocations = $rootScope.Joblocations;

    function initAll() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 10,
            mapTypeControl: true,
            center: new google.maps.LatLng(34.051252, -84.721107),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var num_markers = Joblocations.length;
        // $scope.num_markers = Joblocations.length;
        for (var i = 0; i < num_markers; i++) {
            latlngset = new google.maps.LatLng(Joblocations[i][2], Joblocations[i][3]);
            var marker = new google.maps.Marker({
                map: map,
                title: Joblocations[i][0],
                position: latlngset
            });
            map.setCenter(marker.getPosition())
            var content = "Job title: " + Joblocations[i][0] + ',</br></h3>' + "Contractor: " + Joblocations[i][4] + ',</br></h3>' + "Address: " + Joblocations[i][1] + "</br>" + "<a href=#/officer-viewjobs/" + Joblocations[i][5] + ">" + "Details</a>"
            var infowindow = new google.maps.InfoWindow()
            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));
        }
    };
    if ($location.$$path == '/addlead') {
        Map.init();
        $scope.addleadout = true;
        userFactory.getOfficersList($scope);
    }
    if ($location.$$path == '/officer-activejob') {
        userFactory.viewActiveJobs($scope);
        $timeout(function () {
            init();
        }, 1000);
        $scope.activejobTab = true;
    }
    if ($location.$$path == '/officer-alljobs') {
        userFactory.viewAllJobsmap($scope);
        $timeout(function () {
            initAll();
        }, 2200);
        $scope.jobTab = true;
    }
    /*------------- call view officer factory for view profile ----------*/
    $scope.id = localStorage.getItem('id');
    if ($location.$$path == '/officer-viewprofile' || $location.$$path == '/officer-updateprofile') {
        userFactory.viewOfficer($scope, $scope.id);
        // $scope.jobsTab = true;
    };
    if ($location.$$path == '/officer-addjob') {
        $scope.jobsTab = true;
        Map.init();
    };
    var jobId = $routeParams.id;
    if ($location.$$path == '/officer-editjobs/' + jobId) {
        $rootScope.contractorseditlist = [];
        $scope.myObject = [];
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
        Map.init();
        userFactory.getAllcontractorsEditjob($scope, jobId);
        userFactory.geteditJobType($scope, jobId);
    };
    /*----------code for map search -----------*/
    $scope.search = function (searchPlace) {
        if ($scope.address == undefined) {
            $scope.address = "";
        }
        if ($scope.jobCity == undefined) {
            $scope.jobCity = "";
        }
        if ($scope.jobZipcode == undefined) {
            $scope.jobZipcode = "";
        }
        if ($scope.jobState == undefined) {
            $scope.jobState = "";
        }
        $scope.address1 = $scope.address + " " + $scope.jobCity + " " + $scope.jobZipcode + " " + $scope.jobState;
        console.log($scope.address1);
        $scope.apiError = false;
        Map.search($scope.address1).then(function (res) { // success
            Map.addMarker(res);
            localStorage.setItem('lat', res.geometry.location.lat());
            localStorage.setItem('lng', res.geometry.location.lng());
        }, function (status) { // error
            $scope.apiError = true;
            $scope.apiStatus = status;
        });
    };
    /*----------code for map search -----------*/
    $scope.search1 = function (searchPlace) {
        if ($scope.address == undefined) {
            $scope.address = "";
        }
        if ($scope.jobCity == undefined) {
            $scope.jobCity = "";
        }
        if ($scope.jobZipcode == undefined) {
            $scope.jobZipcode = "";
        }
        if ($scope.jobState == undefined) {
            $scope.jobState = "";
        }
        $scope.address1 = $scope.address + " " + $scope.jobCity + " " + $scope.jobZipcode + " " + $scope.jobState;
        console.log($scope.address1);
        $scope.apiError = false;
        Map.search($scope.address1).then(function (res) { // success
            Map.addMarker(res);
            localStorage.setItem('lat', res.geometry.location.lat());
            localStorage.setItem('lng', res.geometry.location.lng());
        }, function (status) { // error
            $scope.apiError = true;
            $scope.apiStatus = status;
        });
    };
    if ($location.$$path == '/officer-addjob') {
        userFactory.mapSearch($scope);
    };
    /*----------- call office list factory for office list ------------*/
    if ($location.$$path == '/officer-updateprofile') {
        userFactory.getOfficeList($scope);
        userFactory.getallContractors($scope);
    };
    if ($location.$$path == '/officer-addjob' || $location.$$path == '/officer-jobs') {
        userFactory.getallContractors($scope);
        userFactory.getOfficeList($scope);
        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [
            [1, 'desc']
        ]);
    };
    /*------------- call edit officer factory----------*/
    $scope.editOfficers = function (isValid) {
        if (isValid) {
            userFactory.editOfficer($scope, $scope.id);
        };
    };

    if(localStorage.getItem('profileImage')){
        $scope.service_get_image = localStorage.getItem('profileImage');
    }else{
        $scope.service_get_image = 'images/user_payment.png';
    }

    /*-------- update image ----------*/
    $('.uploadForm').submit(function (event, scope) {
        $scope.status_load = true;
        $scope.uploadId = localStorage.getItem('id');
        //stop submit the form, we will post it manually.
        event.preventDefault();
        // Get form
        var form = $(this)[0];
        var data = new FormData(form);
        $.ajax({
            type: "PUT",
            enctype: 'multipart/form-data',
            url: JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            headers: { 'Authorization': localStorage.getItem('jwt_token') },
            success: function (response) {

                $scope.service_get_image = response[0];
                localStorage.setItem('profileImage', $scope.service_get_image);
                $scope.image = true;
                if(localStorage.getItem('profileImage')){
                    $scope.service_get_image = localStorage.getItem('profileImage');
                }else{
                    $scope.service_get_image = 'images/user_payment.png';
                }
                
                apiURL = JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId;
                 var data = {
                    profile_image:  $scope.service_get_image
                };
                $http({
                    method: 'POST',
                    url: apiURL,
                    data: data,
                    headers: { 'Authorization': localStorage.getItem('jwt_token') }
                }).then(function (response) {
                    if(response.data.status){
                       /* localStorage.setItem('MSG', 'Profile image updated successfully!');
                        userService.responseMsg($scope);*/
                    }else{
                        /*localStorage.setItem('MSG', 'Error in updated profile image!');
                        userService.responseMsg($scope);*/
                    }
                   // console.log(response);
                });
                $scope.$apply();
                jQuery('#Modeluploadphoto').modal('hide');
                jQuery('#Modeluploadphoto').find('form')[0].reset();
            },
            error: function (e) {}
        });
    });
   
    var jobId = $routeParams.id;
    $scope.jobId = $routeParams.id;
    if ($location.$$path == '/officer-viewjobs/' + jobId) {
        Map.init();
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
        userFactory.geteditJobType($scope, jobId);
        userFactory.getAllcontractorsEditjob($scope, jobId);
    };
    if ($location.$$path == '/officer-jobs' || $location.$$path == '/officer-viewjobs/' + jobId || $location.$$path == '/officer-editjobs/' + jobId) {
        $scope.jobsTab = true;
    };
    var contractorId = $routeParams.id;
    if ($location.$$path == '/officer-contractors' || $location.$$path == '/officer-viewcontractors/' + contractorId) {
        $scope.contractorTab = true;
    };
    if ($location.$$path == '/officer-jobs') {
        $scope.myObject = [];
        $rootScope.contractorseditlist = [];
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
        userFactory.getJobs($scope);
    };
    /*-----------call factory for get job number----------*/
    if ($location.$$path == '/officer-addjob') {
        userFactory.getJobNumber($scope);
    }
    /*----------call add job factory----------*/
    $scope.addJob = function (isValid) {
        if (isValid) {
            userFactory.addJob($scope, $scope.jobNumber);
        }
    };
    /*----------call add Lead factory----------*/
    $scope.addlead = function (isValid) {
        $scope.finalstatus = true;
        if (isValid) {
            userFactory.addLead($scope);
        }
    };
    $scope.editlead = function (isValid) {
        if (isValid) {
            userFactory.editLead($scope);
        }
    };
    /*------------ functionality for leads ------------*/
    if ($location.$$path == '/officer-addlead') {
        userFactory.getOfficersList($scope);
        $scope.leadtracker = true;
        Map.init();
    }
    if ($location.$$path == '/officer-leads') {
        $scope.leadtracker = true;
        userFactory.getallLead($scope);
    }
    var leadId = $routeParams.id;
    if ($location.$$path == '/officer-viewlead/' + leadId || $location.$$path == '/officer-editlead/' + leadId) {
        $scope.leadtracker = true;
        userFactory.viewLead($scope, leadId);
        userFactory.getOfficersList($scope);
        Map.init();
    }
    $scope.addleadNote = function () {
        if (jQuery("#add-note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Message ").show();
            $(document).on('keyup', '#add-note .note-editable', function () {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.addleadNotes($scope, $scope.Loginid, leadId);
        }
    };
    $scope.editleadNote = function (notes) {
        $('#summernote2').summernote();
        console.log(notes);
        $('#edit_note .note-editable').html(notes.note);
        $scope.edit_note_id = notes.id;
        $scope.edit_lead_id = notes.lead_id;
        console.log($scope.edit_note_id, $scope.edit_lead_id);
        jQuery('#ModeleditComment').modal('show');
    };
    $scope.editleadNotesave = function () {
        if (jQuery("#edit_note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Note").show();
            $(document).on('keyup', '#edit_note .note-editable', function () {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.editleadNotesave($scope);
        }
    };
    $scope.changeleadstatus = function (lead_id, lead_status) {
        if (lead_status == 0) {
            var status = 1;
        }
        if (lead_status == 1) {
            var status = 0;
        }
        $http({
            method: 'Put',
            url: JOBTRACKING_CONSTANT.URL + "/admin/leadStatus/" + lead_id,
            data: {
                status: status,
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }    
        }).then(function (response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Lead status successfully changed!');
                userService.responseMsg($scope);
                userFactory.getallLead($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in change lead status!');
                userService.responseMsg($scope);
            }
        });
    };
    /*------------- call view jobs factory----------*/
    if ($location.$$path == '/officer-viewjobs/' + jobId || $location.$$path == '/officer-editjobs/' + jobId) {
        userFactory.viewJobs($scope, jobId);
        userFactory.getallContractors($scope);
        userFactory.viewNotes($scope, jobId);
    };
    /*------------- call edit jobs factory----------*/
    $scope.editJob = function (isValid) {
        if (isValid) {
            userFactory.editJobs($scope, jobId);
        }
    };
    $scope.getJobId = function (jobsarciveId) {
        $scope.jobsarciveId = jobsarciveId;
        jQuery('.jobs_archive_pop').modal('show');
    };
    $scope.archiveJobs = function () {
        userFactory.archiveJobs($scope, $scope.jobsarciveId);
    };
    $scope.resumeJobs = function () {
        userFactory.resumeJobs($scope, $scope.jobsarciveId);
    };
    $scope.getDeleteJobId = function (jobsDeleteId) {
        $scope.jobsDeleteId = jobsDeleteId;
        jQuery('.jobs_delete_pop').modal('show');
    };
    $scope.jobDelete = function ($scope, ) {
        userFactory.deleteJobs($scope, $scope.jobsDeleteId);
    };
    /*--------------for get job type call factory-----------*/
    var jobTypeId = $routeParams.id;
    if ($location.$$path == '/officer-addjob' || $location.$$path == '/officer-jobs' || $location.$$path == '/officer-editjobs/' + jobTypeId) {
        userFactory.getJobType($scope);
    };
    /*------------ call get all contractor factory ------------*/
    if ($location.$$path == '/officer-contractors') {
        userFactory.getallContractors($scope);
        $scope.contractorTab = true;
    };
    if ($location.$$path == '/officer-addcontractors') {
        $scope.contractorTab = true;
    };
    /*------------- call add contractor factory----------*/
    $scope.addContractor = function (isValid) {
        if (isValid) {
            userFactory.addContractors($scope);
        };
    };
    /*------------- call view contractor factory----------*/
    var contractorId = $routeParams.id;
    if ($location.$$path == '/officer-viewcontractors/' + contractorId || $location.$$path == '/officer-editcontractors/' + contractorId) {
        userFactory.viewContractors($scope, contractorId);
        $scope.contractorTab = true;
    }
    $scope.getDeleteContractorId = function (contractorDeleteId) {
        $scope.contractorDeleteId = contractorDeleteId;
        jQuery('.contractors_delete_pop').modal('show');
    };
    /*------------- call delete contractor factory----------*/
    $scope.contractorsDelete = function () {
        userFactory.deleteContractors($scope, $scope.contractorDeleteId);
    };
    /*------------- call edit contractor factory----------*/
    $scope.editContractors = function (isValid) {
        if (isValid) {
            userFactory.editContractors($scope, contractorId);
        }
    };
    /*------------ call view all contractor factory ------------*/
    if ($location.$$path == '/officer-viewcontractors/' + contractorId) {
        userFactory.viewContractors($scope, contractorId);
    };
    /*-------------- Post comment on contractor profile ---------*/
    $scope.postComment = function () {
        $scope.commentId = contractorId;
        jQuery('.comment_pop').modal('show');
    };
    /*--------call post comment factory--------*/
    $scope.postC = function () {
        $scope.cname = $scope.comment;
        jQuery('.comment_pop').modal('hide');
    };
    /*-------------- show job list select box--------------*/
    $scope.jobType = function (selectedType) {
        if (selectedType) {
            userFactory.getJobByType($scope, selectedType);
        }
    };
    $scope.contractorType = function (selectedType) {
        if (selectedType) {
            userFactory.getJobByContractor($scope, selectedType);
        } else {
            userFactory.getJobs($scope);
        }
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.setDate = function (year, month, day) {
        $scope.start_date = new Date(year, month, day);
        $scope.end_date = new Date(year, month, day);
    };
    $scope.format = 'MM/dd/yyyy';
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.setDate = function (year, month, day) {
        $scope.start_date = new Date(year, month, day);
        $scope.end_date = new Date(year, month, day);
    };
    $scope.format = 'MM/dd/yyyy';
    $scope.popup2 = {
        opened: false
    };
    $scope.Userlogout = function () {
        localStorage.removeItem('id');
        localStorage.removeItem('type');
        localStorage.removeItem('login_user_name');
        $location.path('/');
    }
    $scope.getEdittime = function (eidt_job_detail) {
        $scope.editStartdate = new Date(eidt_job_detail.created_at);
        $scope.edit_contractor_id = String(eidt_job_detail.contractor_id);
        $scope.edittimeId = eidt_job_detail.id;
        $scope.edit_no_worker = eidt_job_detail.crew_count;
        $scope.edit_start_date = new Date(eidt_job_detail.created_at);
        if (eidt_job_detail.updated_at != null) {
            $scope.edit_end_date = new Date(eidt_job_detail.updated_at);
            $scope.editEnddate = new Date(eidt_job_detail.updated_at);
        } else {
            $scope.edit_end_date = "";
            $scope.editEnddate = "";
            // var editdate = moment(eidt_job_detail.created_at).format("L") + " " + moment($scope.end_time).format("HH:mm");
            // $scope.editEnddate = new Date(editdate);
            // console.log($scope.editEnddate);
        }
    };
    $scope.cleardate = function () {
        $scope.edit_end_date = "";
        $scope.editEnddate = "";
    };
    $scope.cleartime = function () {
        $scope.editEnddate = "";
    };
    $scope.editTime = function () {
        userFactory.editJobtime($scope, $scope.edittimeId, jobId);
    };
    $scope.addJobhistory = function () {
        userFactory.addJobHistory($scope, jobId);
    };
    /*------------- call function for notes function factory----------*/
    $scope.addNote = function () {
        alert('hello');
        if (jQuery("#add-note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Message ").show();
            $(document).on('keyup', '#add-note .note-editable', function () {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.addNotes($scope, $scope.Loginid, jobId);
        }
    };
    $scope.myObject = [];
    $scope.ContractorArray = function (contractorId, contractorName, lastName) {
        if (jQuery("#" + contractorId).prop("checked") == true) {
            var nd = {
                contractor_id: contractorId,
                contractorName: contractorName + " " + lastName,
            };
            $scope.myObject.push(nd);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $scope.myObject.splice($.inArray(contractorId, contractorName, $scope.myObject), 1);
        }
        console.log($scope.myObject);
    };
    $scope.editContractorArray = function (contractorId, contractorName, lastName) {
        if (jQuery("#" + contractorId).prop("checked") == true) {
            var nd = {
                contractor_id: contractorId,
                contractorName: contractorName + " " + lastName,
            };
            $rootScope.contractorseditlist.push(nd);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $rootScope.contractorseditlist.splice($.inArray(contractorId, contractorName, $scope.myObject), 1);
        }
        console.log($rootScope.contractorseditlist);
    };
    $scope.myObject2 = [];
    $scope.myObject3 = [];
    $scope.jobsArray = function (jobName) {
        if (jQuery("#" + jobName).prop("checked") == true) {
            var nd = {
                jobName: jobName,
            };
            $scope.myObject2.push(jobName);
            $scope.myObject3.push(nd);
        } else {
            jQuery("." + jobName).prop("checked", false);
            $scope.myObject2.splice($.inArray(jobName, $scope.myObject2), 1);
            $scope.myObject3.splice($.inArray(jobName, $scope.myObject3), 1);
        }
        console.log(JSON.stringify($scope.myObject2));
    };
    $scope.editjobtypeArray = function (jobName) {
        if (jQuery("#" + jobName).prop("checked") == true) {
            var nd = {
                jobName: jobName,
            };
            $rootScope.jobtypeEditlistcheck.push(nd);
            $rootScope.myobject4.push(jobName);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $rootScope.myobject4.splice($.inArray(jobName, $rootScope.myobject4), 1);
            $rootScope.jobtypeEditlistcheck.splice($.inArray(jobName, $rootScope.jobtypeEditlistcheck), 1);
        }
        console.log($rootScope.myobject4);
        console.log($rootScope.jobtypeEditlistcheck);
    };
    if ($location.$$path == '/officer-changepassword') {
        $scope.cpassword = '0';
        $scope.getCurrentPass = function () {
            $scope.id = localStorage.getItem('id');
            console.log($scope.id);
            apiURL = JOBTRACKING_CONSTANT.URL + "/admin/changePassword/" + $scope.id;
            var data = {
                password: $scope.cpass
            };
            $http({
                method: 'POST',
                url: apiURL,
                data: data,
                headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function (response) {
                if (response.data.status) {
                    console.log(response.data.status);
                    $scope.cpassword = '1';
                } else {
                    $scope.cpassword = '0';
                    $scope.msg = response.data.msg;
                }
            });
        };
        $scope.changePassword = function (isValid) {
            if (isValid) {
                $scope.id = localStorage.getItem('id');
                apiURL = JOBTRACKING_CONSTANT.URL + "/admin/changePassword/" + $scope.id;
                var data = {
                    password: $scope.password
                };
                if ($scope.cnfPassword != undefined) {
                    $http({
                        method: 'PUT',
                        url: apiURL,
                        data: data,
                        headers: { 'Authorization': localStorage.getItem('jwt_token') }
                    }).then(function (response) {
                        if (response.data.status) {
                            console.log(response.data);
                            $location.url('/officer-jobs');
                            localStorage.setItem('MSG', 'Your password updated successfully!');
                            userService.responseMsg($scope);
                        } else {
                            localStorage.setItem('ErrMSG', 'Your password is not updated!');
                            userService.responseMsg($scope);
                        }
                    });
                }
            }
        };
    }
    $scope.editNote = function (notes) {
        $('#summernote2').summernote();
        $('#edit_note .note-editable').html(notes.note);
        $scope.edit_note_id = notes.id;
        $scope.edit_job_id = notes.job_id;
        jQuery('#ModeleditComment').modal('show');
    };
    $scope.editNotesave = function () {
        if (jQuery("#edit_note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Note").show();
            $(document).on('keyup', '#edit_note .note-editable', function () {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.editNotesave($scope);
        }
    };
    $scope.delteNote = function (n_ID, job_id) {
        $scope.n_id = n_ID;
        $scope.job_id = job_id;
        jQuery('#ModelDeleteComment').modal('show');
    };
    $scope.deletenote = function (n_ID, job_id) {
        userFactory.deletenote($scope, n_ID, job_id);
    };
    $('#summernote').summernote();
});