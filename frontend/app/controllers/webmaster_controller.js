app.controller('webmaster', function($scope, Map, userFactory, $http, $rootScope, $timeout, $location, JOBTRACKING_CONSTANT, userService, DTOptionsBuilder, DTColumnBuilder, $routeParams) {
    $scope.finalstatus = true;
    if (jQuery(window).width() < 768) {
        $scope.collapseClass = 'collapse';
    } else {
        $scope.collapseClass = '';
    }
    /*----------webmaster login factory call---------*/
    if (localStorage.getItem('type') != 1 && (localStorage.getItem('id') == '' || localStorage.getItem('id') == null || localStorage.getItem('id') == undefined)) {
        $location.path('/webmaster');
    }
    $scope.loginType = localStorage.getItem('type');
    $scope.adminLoginid = localStorage.getItem('id');
    $scope.Loginid = localStorage.getItem('id');
    $scope.adminLogin = function() {
        userFactory.webmasterLogin($scope);
    };

    /*$scope.wemaster_id = localStorage.getItem('id');
    if ($location.$$path == '/webmaster-viewProfile' || $location.$$path == '/webmaster-updateprofile') {
        userFactory.viewContractors($scope, $scope.wemaster_id);
    };*/

    var officerId = $routeParams.id;
    if ($location.$$path == '/webmaster-offices' || $location.$$path == '/webmaster-addofficers' || $location.$$path == '/webmaster-editofficers/' + officerId || $location.$$path == '/webmaster-addcontractors' || $location.$$path == '/webmaster-editcontractors/' + officerId) {
        userFactory.getOfficeList($scope);

    }

    var officeId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewoffice/' + officeId || $location.$$path == '/webmaster-editoffice/' + officeId) {
        userFactory.viewOffice($scope, officeId);
        $scope.officesTab = true;
    }

    /*----------tabs active condition---------*/

    if ($location.$$path == '/webmaster-officers' || $location.$$path == '/webmaster-addofficers') {
        $scope.officersTab = true;
    }
    if ($location.$$path == '/webmaster-offices' || $location.$$path == '/webmaster-addoffice') {
        $scope.officesTab = true;
    }
    if ($location.$$path == '/webmaster-contractors' || $location.$$path == '/webmaster-addcontractors') {
        $scope.contractorTab = true;
    }
    if ($location.$$path == '/webmaster-job') {
        $scope.jobTab = true;
        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [
            [1, 'desc']
        ]);
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
    }
    if ($location.$$path == '/webmaster-addjob') {
        Map.init();
        $scope.jobTab = true;

    }

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.setDate = function(year, month, day) {
        $scope.start_date = new Date(year, month, day);
        $scope.end_date = new Date(year, month, day);
    };
    $scope.format = 'MM/dd/yyyy';

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.format = 'MM/dd/yyyy';

    $scope.popup2 = {
        opened: false
    };

    $scope.backpage = function() {
        window.history.back();
    }

    /*-------------multiple locations point out----------*/

    var locations = $rootScope.locations;

    var map;
    var markers = [];

    function init() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 15,
            mapTypeControl: true,
            center: new google.maps.LatLng(34.0512519, -84.7211074)
        });

        var num_markers = locations.length;
        for (var i = 0; i < num_markers; i++) {


            latlngset = new google.maps.LatLng(locations[i][2], locations[i][3]);


            var marker = new google.maps.Marker({
                map: map,
                title: locations[i][0],
                position: latlngset
            });
            map.setCenter(marker.getPosition())


            var content = "Job title: " + locations[i][0] + ',</br></h3>' + "Contractor: " + locations[i][4] + ',</br></h3>' + "Address: " + locations[i][1] +
                "</br>" + "<a href=#/webmaster-viewjob/" + locations[i][5] + ">" +
                "Details</a>"

            var infowindow = new google.maps.InfoWindow()

            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));

        }
    };

    var Joblocations = $rootScope.Joblocations;

    function initAll() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 10,
            mapTypeControl: true,
            center: new google.maps.LatLng(34.051252, -84.721107)
        });

        var num_markers = Joblocations.length;
        // $scope.num_markers = Joblocations.length;
        console.log(num_markers);
        for (var i = 0; i < num_markers; i++) {

            latlngset = new google.maps.LatLng(Joblocations[i][2], Joblocations[i][3]);
            var marker = new google.maps.Marker({
                map: map,
                title: Joblocations[i][0],
                position: latlngset
            });
            map.setCenter(marker.getPosition());

            var content = "Job title: " + Joblocations[i][0] + ',</br></h3>' + "Contractor: " + Joblocations[i][4] + ',</br></h3>' + "Address: " + Joblocations[i][1] +
                "</br>" + "<a href=#/webmaster-viewjob/" + Joblocations[i][5] + ">" +
                "Details</a>"

            var infowindow = new google.maps.InfoWindow()

            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));

        }
    };

    if ($location.$$path == '/webmaster-activejob') {
        userFactory.viewActiveJobs($scope);
        $timeout(function() {
            init();
        }, 1000);

        $scope.activejobTab = true;
    }

    if ($location.$$path == '/webmaster-alljobs') {
        userFactory.viewAllJobsmap($scope);
        $timeout(function() {
            initAll();
        }, 2200);

        $scope.jobTab = true;
    }

    var jobId = $routeParams.id;
    $scope.jobId = $routeParams.id;
    if ($location.$$path == '/webmaster-editjob/' + jobId) {
        $rootScope.contractorseditlist = [];
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
        Map.init();
        userFactory.getAllcontractorsEditjob($scope, jobId);
    }
    if ($location.$$path == '/webmaster-viewjob/' + jobId) {
        Map.init();
        userFactory.getAllcontractorsEditjob($scope, jobId);
    }

    if ($location.$$path == '/webmaster-addjobtype' || $location.$$path == '/webmaster-jobtype') {
        $scope.jobTypeTab = true;
    }



    /*----------code for map search -----------*/
    $scope.search = function(searchPlace) {
        if ($scope.address == undefined) {
            $scope.address = "";
        }
        if ($scope.jobCity == undefined) {
            $scope.jobCity = "";
        }
        if ($scope.jobZipcode == undefined) {
            $scope.jobZipcode = "";
        }
        if ($scope.jobState == undefined) {
            $scope.jobState = "";
        }

        $scope.address1 = $scope.address + " " + $scope.jobCity + " " + $scope.jobZipcode + " " + $scope.jobState;
        console.log($scope.address1);
        $scope.apiError = false;
        Map.search($scope.address1)
            .then(
                function(res) { // success
                    Map.addMarker(res);
                    console.log(res.geometry.location.lat(), res.geometry.location.lng());
                    localStorage.setItem('lat', res.geometry.location.lat());
                    localStorage.setItem('lng', res.geometry.location.lng());
                },
                function(status) { // error
                    $scope.apiError = true;
                    $scope.apiStatus = status;
                }
            );
    };

    /*----------code for map search -----------*/
    $scope.search1 = function(searchPlace) {
        if ($scope.address == undefined) {
            $scope.address = "";
        }
        if ($scope.jobCity == undefined) {
            $scope.jobCity = "";
        }
        if ($scope.jobZipcode == undefined) {
            $scope.jobZipcode = "";
        }
        if ($scope.jobState == undefined) {
            $scope.jobState = "";
        }

        $scope.address1 = $scope.address + " " + $scope.jobCity + " " + $scope.jobZipcode + " " + $scope.jobState;
        console.log($scope.address1);
        $scope.apiError = false;
        Map.search($scope.address1)
            .then(
                function(res) { // success
                    Map.addMarker(res);
                    localStorage.setItem('lat', res.geometry.location.lat());
                    localStorage.setItem('lng', res.geometry.location.lng());
                },
                function(status) { // error
                    $scope.apiError = true;
                    $scope.apiStatus = status;
                }
            );
    };

    /*---------------call add office factory----------*/
    if ($location.$$path == '/webmaster-addjob') {
        userFactory.mapSearch($scope);
    };

    $scope.addOffice = function(isValid) {
        if (isValid) {
            userFactory.addOffice($scope);
        };
    };

    /*---------------call edit office factory----------*/

    $scope.editOffice = function(isEditValid) {
        if (isEditValid) {
            userFactory.editOffice($scope, officeId);
        };
    };

     $scope.editWebmasters = function(isValid) {
        if (isValid) {
            $scope.id = localStorage.getItem('id');
            userFactory.editWebmasters($scope, $scope.id);
        }
    };

     if(localStorage.getItem('profileImage')){
        $scope.service_get_image = localStorage.getItem('profileImage');
    }else{
        $scope.service_get_image = 'images/user_payment.png';
    }

    /*-------- update image ----------*/
    $('.uploadForm').submit(function (event, scope) {
        $scope.status_load = true;
        $scope.uploadId = localStorage.getItem('id');
        //stop submit the form, we will post it manually.
        event.preventDefault();
        // Get form
        var form = $(this)[0];
        var data = new FormData(form);
        $.ajax({
            type: "PUT",
            enctype: 'multipart/form-data',
            url: JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            headers: { 'Authorization': localStorage.getItem('jwt_token') },
            success: function (response) {

                $scope.service_get_image = response[0];
                localStorage.setItem('profileImage', $scope.service_get_image);
                $scope.image = true;
                if(localStorage.getItem('profileImage')){
                    $scope.service_get_image = localStorage.getItem('profileImage');
                }else{
                    $scope.service_get_image = 'images/user_payment.png';
                }
                
                apiURL = JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId;
                 var data = {
                    profile_image:  $scope.service_get_image
                };
                $http({
                    method: 'POST',
                    url: apiURL,
                    data: data,
                    headers: { 'Authorization': localStorage.getItem('jwt_token') }
                }).then(function (response) {
                    if(response.data.status){
                       /* localStorage.setItem('MSG', 'Profile image updated successfully!');
                        userService.responseMsg($scope);*/
                    }else{
                        /*localStorage.setItem('MSG', 'Error in updated profile image!');
                        userService.responseMsg($scope);*/
                    }
                   // console.log(response);
                });
                $scope.$apply();
                jQuery('#Modeluploadphoto').modal('hide');
                jQuery('#Modeluploadphoto').find('form')[0].reset();
            },
            error: function (e) {}
        });
    });

    $scope.getOfficeId = function(id) {
        $scope.oId = id;
        jQuery('.delete_pop').modal('show');
    };

    /*------------ call delete office factory ------------*/

    $scope.officeDelete = function() {
        userFactory.deleteOffice($scope, $scope.oId);
    };



    /*------------ call get all officers factory ------------*/

    if ($location.$$path == '/webmaster-officers') {
        userFactory.getOfficersList($scope);
    }

    /*------------ call add officer factory ------------*/

    $scope.addOfficers = function(isValid) {
        if (isValid) {
            userFactory.addOfficer($scope);
        }
    };

    /*------------ call view officer factory ------------*/

    var officerId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewofficers/' + officerId || $location.$$path == '/webmaster-editofficers/' + officerId) {
        userFactory.viewOfficer($scope, officerId);
        $scope.officersTab = true;
    }

    /*------------- call edit officer factory----------*/

    $scope.editOfficers = function(isValid) {
        if (isValid) {
            userFactory.editOfficer($scope, officerId);

        };
    };

    $scope.getDeleteOfficerId = function(officersDeleteId) {
        $scope.officersDeleteId = officersDeleteId;
        jQuery('.officers_delete_pop').modal('show');
    };

    /*------------- call delete officer factory----------*/

    $scope.officersDelete = function() {
        userFactory.deleteOfficer($scope, $scope.officersDeleteId);
    };

    /*------------- call get all contractor factory----------*/
    var jobId = $routeParams.id;

    if ($location.$$path == '/webmaster-contractors' || $location.$$path == '/webmaster-job' || $location.$$path == '/webmaster-addjob' || $location.$$path == '/webmaster-editjob/' + jobId) {
        userFactory.getallContractors($scope);
        userFactory.getallOffices($scope);
    }

    /*------------- call add contractor factory----------*/

    $scope.addContractor = function(isValid) {
        if (isValid) {
            userFactory.addContractors($scope);
        };
    };

    /*------------- call view contractor factory----------*/

    var contractorId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewcontractors/' + contractorId || $location.$$path == '/webmaster-editcontractors/' + contractorId) {
        userFactory.viewContractors($scope, contractorId);
        $scope.contractorTab = true;
    }

    $scope.getDeleteContractorId = function(contractorDeleteId) {
        $scope.contractorDeleteId = contractorDeleteId;
        jQuery('.contractors_delete_pop').modal('show');
    };

    /*------------- call delete contractor factory----------*/

    $scope.contractorsDelete = function() {
        userFactory.deleteContractors($scope, $scope.contractorDeleteId);
    };

    /*------------- call edit contractor factory----------*/

    $scope.editContractors = function(isValid) {
        if (isValid) {
            userFactory.editContractors($scope, contractorId);
        }
    };

    /*-----------call factory for get job number----------*/

    if ($location.$$path == '/webmaster-addjob') {
        userFactory.getJobNumber($scope);
    }

    /*----------call add job factory----------*/

    $scope.addJob = function(isValid) {
        if (isValid) {
            userFactory.addJob($scope, $scope.jobNumber);
        }
    };

    /*----------call add Lead factory----------*/

    $scope.addlead = function(isValid) {
        $scope.finalstatus = true;
        if (isValid) {
            userFactory.addLead($scope);
        }
    };

    $scope.editlead = function(isValid) {
        if (isValid) {
            userFactory.editLead($scope);
        }
    };

    /*------------ functionality for leads ------------*/
    if ($location.$$path == '/webmaster-addlead') {
        userFactory.getOfficersList($scope);
        $scope.leadtracker = true;
        Map.init();
    }

    if ($location.$$path == '/webmaster-leads') {
        $scope.leadtracker = true;
        userFactory.getallLead($scope)
    }

    var leadId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewlead/' + leadId || $location.$$path == '/webmaster-editlead/' + leadId) {
        $scope.leadtracker = true;
        userFactory.viewLead($scope, leadId);
        userFactory.getOfficersList($scope);
        Map.init();
    }

    $scope.addleadNote = function() {
        if (jQuery("#add-note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Message ").show();
            $(document).on('keyup', '#add-note .note-editable', function() {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.addleadNotes($scope, $scope.adminLoginid, leadId);
        }

    };

    $scope.editleadNote = function(notes) {
        $('#summernote2').summernote();
        console.log(notes);
        $('#edit_note .note-editable').html(notes.note);
        $scope.edit_note_id = notes.id;
        $scope.edit_lead_id = notes.lead_id;
        console.log($scope.edit_note_id, $scope.edit_lead_id);
        jQuery('#ModeleditComment').modal('show');
    };

    $scope.editleadNotesave = function() {
        if (jQuery("#edit_note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Note").show();
            $(document).on('keyup', '#edit_note .note-editable', function() {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.editleadNotesave($scope);
        }
    };

    // $scope.deleteleadNote = function(n_ID, lead_id) {
    //     $scope.n_id = n_ID;
    //     $scope.lead_id = lead_id;
    //     jQuery('#ModelDeleteComment').modal('show');
    // };

    // $scope.deleteleadnote = function(n_ID, lead_id) {
    //     userFactory.deletenote($scope, n_ID, lead_id);
    // };  

    /*----------call get job factory---------*/

    if ($location.$$path == '/webmaster-job') {
        userFactory.getJobs($scope);
        $scope.myObject = [];
        $rootScope.contractorseditlist = [];
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];
    }

    /*----------call get archive job factory---------*/
    if ($location.$$path == '/webmaster-archivejob') {
        $scope.archivejob = true;
        userFactory.getarchiveJobs($scope);
        $scope.myObject = [];
        $rootScope.contractorseditlist = [];
        $scope.myObject2 = [];
        $scope.myObject3 = [];
        $rootScope.jobtypeEditlistcheck = [];
        $rootScope.myobject4 = [];

    }


    /*------------- call view jobs factory----------*/

    var jobId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewjob/' + jobId || $location.$$path == '/webmaster-editjob/' + jobId) {
        userFactory.viewJobs($scope, jobId);
        userFactory.viewNotes($scope, jobId);
        userFactory.geteditJobType($scope, jobId);
        $scope.jobTab = true;
    }

    $scope.editJob = function(isValid) {
        if (isValid) {
            userFactory.editJobs($scope, jobId);
        }
    };

    $scope.getJobId = function(jobsarciveId) {
        $scope.jobsarciveId = jobsarciveId;
        jQuery('.jobs_archive_pop').modal('show');
    };


    $scope.archiveJobs = function() {
        userFactory.archiveJobs($scope, $scope.jobsarciveId);
    };

    $scope.resumeJobs = function() {
        userFactory.resumeJobs($scope, $scope.jobsarciveId);
    };

    $scope.getDeleteJobId = function(jobsDeleteId) {
        $scope.jobsDeleteId = jobsDeleteId;

        jQuery('.jobs_delete_pop').modal('show');
    };

    $scope.jobDelete = function() {
        userFactory.deleteJobs($scope, $scope.jobsDeleteId);
    };

    /*-----------assign jobs----------*/
    $scope.assignJobs = function(jobId) {
        $scope.job_id = jobId;
        jQuery('.comment_pop').modal('show');
    };

    $scope.postAssignJob = function() {
        userFactory.assignJobs($scope, $scope.job_id);
    };

    /*-------------call factory for add job type------------*/

    $scope.addJobType = function() {
        userFactory.addJobType($scope);
    };

    /*-------------call factory for get job type------------*/

    var jobTypeId = $routeParams.id;
    if ($location.$$path == '/webmaster-jobtype' || $location.$$path == '/webmaster-editjobtype/' + jobTypeId) {
        $scope.jobTypeTab = true;
        $scope.jobval = [];
    }
    if ($location.$$path == '/webmaster-jobtype' || $location.$$path == '/webmaster-job' || $location.$$path == '/webmaster-editjobtype/' + jobTypeId || $location.$$path == '/webmaster-addjob' || $location.$$path == '/webmaster-editjob/' + jobTypeId) {
        userFactory.getJobType($scope);
        $scope.jobtype = localStorage.getItem("jobTitle");
        $scope.jobval = [];
    };

    $scope.takejobtitle = function(jobTitle) {
        localStorage.setItem("jobTitle", jobTitle);
    };

    /*-------------call factory for edit job type------------*/
    $scope.materials = $scope.materialss;
    angular.forEach($scope.materials, function(obj) {
        obj["checked"] = false;
        obj.checked = false;
    });
    $('#artists').bind('click', function(e) {
        $('#artistsbtn #artistslist').slideToggle('fast');
    });


    $scope.getArtistsByArtist = function() {
        $scope.jobval = "";
        angular.forEach($scope.jobtypeList, function(value, key) {
            if (value.checked) {
                // alert(value.checked) 
                // $scope.jobval.push(value.title);
                if ($scope.jobval.length == 0) {
                    $scope.jobval = value.title;
                } else {
                    $scope.jobval += "," + value.title;
                }
            }
            console.log($scope.jobval);
        });

    };

    $scope.changeleadstatus = function(lead_id, lead_status) {
        if (lead_status == 0) {
            var status = 1;
        }
        if (lead_status == 1) {
            var status = 0;
        }
        $http({
            method: 'Put',
            url: JOBTRACKING_CONSTANT.URL + "/admin/leadStatus/" + lead_id,
            data: {
                status: status,
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Lead status successfully changed!');
                userService.responseMsg($scope);
                userFactory.getallLead($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in change lead status!');
                userService.responseMsg($scope);
            }
        });
    };

    $scope.editJobType = function() {
        userFactory.editJobType($scope, jobTypeId);
    };

    /*-------------call factory for edit job type------------*/

    $scope.getJobTypeId = function(jobTypeDeleteId) {
        $scope.jobTypeDeleteId = jobTypeDeleteId;
        jQuery('.jobtype_delete_pop').modal('show');
    };

    $scope.jobTypeDelete = function() {
        userFactory.deleteJobType($scope, $scope.jobTypeDeleteId);
    };

    /*-------------- show job list select box--------------*/
    $scope.jobType = function(selectedType) {
        if (selectedType) {
            userFactory.getJobByType($scope, selectedType);
        }else{
            userFactory.getJobs($scope);
        }
    };
    $scope.contractorType = function(selectedType) {
        if (selectedType) {
            userFactory.getJobByContractor($scope, selectedType);
        }else{
            userFactory.getJobs($scope);
        }
    };


    /*----------------------------------admin code----------------------------------------*/
    var adminId = $routeParams.id;
    if ($location.$$path == '/webmaster-viewadmin/' + adminId || $location.$$path == '/webmaster-editadmin/' + adminId) {
        userFactory.viewAdmins($scope, adminId);
        $scope.adminTab = true;
    }

    if ($location.$$path == '/webmaster-admins' || $location.$$path == '/webmaster-addadmin') {
        $scope.adminTab = true;
    }

    if ($location.$$path == '/webmaster-admins') {
        userFactory.getAdminList($scope, localStorage.getItem('id'));
    }

    if ($location.$$path == '/webmaster-viewProfile' || $location.$$path == '/webmaster-updateprofile') {
        userFactory.viewWebmaster($scope, localStorage.getItem('id'));
    }



    $scope.getDeleteAdminId = function(AdminId) {
        $scope.AdminDeleteId = AdminId;
        jQuery('.Admin_delete_pop').modal('show');
    };



    /*------------- call delete Admin factory----------*/

    $scope.adminsDelete = function() {
        userFactory.deleteAdmins($scope, $scope.AdminDeleteId);
    };
    /*------------- call add Admin factory----------*/

    $scope.addAdmin = function(isValid) {
        if (isValid) {
            userFactory.addAdmins($scope);
        };
    };

    $scope.editAdmin = function(isValid) {
        if (isValid) {
            userFactory.editAdmin($scope, adminId);
        }
    };

    $scope.Userlogout = function() {
        localStorage.removeItem('id');
        localStorage.removeItem('type');
        $location.path('/webmaster');
    }
    /*------------- call function for notes function factory----------*/
    $scope.addNote = function() {
        if (jQuery("#add-note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Message ").show();
            $(document).on('keyup', '#add-note .note-editable', function() {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.addNotes($scope, $scope.adminLoginid, jobId);
        }

    };

    $scope.editNote = function(notes) {
        $('#summernote2').summernote();
        $('#edit_note .note-editable').html(notes.note);
        $scope.edit_note_id = notes.id;
        $scope.edit_job_id = notes.job_id;

        jQuery('#ModeleditComment').modal('show');
    };
    $scope.editNotesave = function() {
        if (jQuery("#edit_note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Note").show();
            $(document).on('keyup', '#edit_note .note-editable', function() {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.editNotesave($scope);
        }
    };

    $scope.delteNote = function(n_ID, job_id) {
        $scope.n_id = n_ID;
        $scope.job_id = job_id;
        jQuery('#ModelDeleteComment').modal('show');
    };

    $scope.deletenote = function(n_ID, job_id) {
        userFactory.deletenote($scope, n_ID, job_id);
    };
    $scope.getEdittime = function(eidt_job_detail) {

        $scope.editStartdate = new Date(eidt_job_detail.created_at);
        $scope.edit_contractor_id = String(eidt_job_detail.contractor_id);
        $scope.edittimeId = eidt_job_detail.id;
        $scope.edit_no_worker = eidt_job_detail.crew_count;
        $scope.edit_start_date = new Date(eidt_job_detail.created_at);

        if (eidt_job_detail.updated_at != null) {
            $scope.edit_end_date = new Date(eidt_job_detail.updated_at);
            $scope.editEnddate = new Date(eidt_job_detail.updated_at);
        } else {
            $scope.edit_end_date = "";
            $scope.editEnddate = "";
            // var editdate = moment(eidt_job_detail.created_at).format("L") + " " + moment($scope.end_time).format("HH:mm");
            // $scope.editEnddate = new Date(editdate);
            // console.log($scope.editEnddate);
        }
    };
    $scope.cleardate = function() {
        $scope.edit_end_date = "";
        $scope.editEnddate = "";
    };
    $scope.cleartime = function() {
        $scope.editEnddate = "";
    };
    $scope.editTime = function() {
        userFactory.editJobtime($scope, $scope.edittimeId, jobId);
    };
    $scope.addJobhistory = function() {
        userFactory.addJobHistory($scope, jobId);
    };

    $scope.myObject = [];
    $scope.ContractorArray = function(contractorId, contractorName, lastName) {
        if (jQuery("#" + contractorId).prop("checked") == true) {
            var nd = {
                contractor_id: contractorId,
                contractorName: contractorName + " " + lastName,
            };
            $scope.myObject.push(nd);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $scope.myObject.splice($.inArray(contractorId, contractorName, $scope.myObject), 1);
        }
        console.log($scope.myObject);
    };

    $scope.editContractorArray = function(contractorId, contractorName, lastName) {
        if (jQuery("#" + contractorId).prop("checked") == true) {
            var nd = {
                contractor_id: contractorId,
                contractorName: contractorName + " " + lastName,
            };
            $rootScope.contractorseditlist.push(nd);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $rootScope.contractorseditlist.splice($.inArray(contractorId, contractorName, $scope.myObject), 1);
        }
        console.log($rootScope.contractorseditlist);
    };

    $scope.myObject2 = [];
    $scope.myObject3 = [];
    $scope.jobsArray = function(jobName) {
        if (jQuery("#" + jobName).prop("checked") == true) {
            var nd = {
                jobName: jobName,
            };
            $scope.myObject2.push(jobName);
            $scope.myObject3.push(nd);
        } else {
            jQuery("." + jobName).prop("checked", false);
            $scope.myObject2.splice($.inArray(jobName, $scope.myObject2), 1);
            $scope.myObject3.splice($.inArray(jobName, $scope.myObject3), 1);
        }
        console.log(JSON.stringify($scope.myObject2));
    };

    $scope.editjobtypeArray = function(jobName) {
        if (jQuery("#" + jobName).prop("checked") == true) {
            var nd = {
                jobName: jobName,
            };
            $rootScope.jobtypeEditlistcheck.push(nd);
            $rootScope.myobject4.push(jobName);
        } else {
            jQuery("." + contractorId).prop("checked", false);
            $rootScope.myobject4.splice($.inArray(jobName, $rootScope.myobject4), 1);
            $rootScope.jobtypeEditlistcheck.splice($.inArray(jobName, $rootScope.jobtypeEditlistcheck), 1);
        }
        console.log($rootScope.myobject4);
        console.log($rootScope.jobtypeEditlistcheck);
    };


    if ($location.$$path == '/webmaster-changepassword') {
        $scope.cpassword = '0';
        $scope.getCurrentPass = function() {
            $scope.id = localStorage.getItem('id');
            console.log($scope.id);
            apiURL = JOBTRACKING_CONSTANT.URL + "/admin/changePassword/" + $scope.id;
            var data = {
                password: $scope.cpass
            };

            $http({
                method: 'POST',
                url: apiURL,
                data: data,
                headers: { 'Authorization': localStorage.getItem('jwt_token') } 
            }).then(function(response) {

                if (response.data.status) {
                    console.log(response.data.status);
                    $scope.cpassword = '1';

                } else {
                    $scope.cpassword = '0';
                    $scope.msg = response.data.msg;
                }
            });
        };

        $scope.changePassword = function(isValid) {
         if(isValid){
            $scope.id = localStorage.getItem('id');
            apiURL = JOBTRACKING_CONSTANT.URL + "/admin/changePassword/" + $scope.id;
            var data = {
                password: $scope.password
            };

            if ($scope.cnfPassword != undefined) {
                $http({
                    method: 'PUT',
                    url: apiURL,
                    data: data,
                    headers: { 'Authorization': localStorage.getItem('jwt_token') }
                }).then(function(response) {

                    if (response.data.status) {
                        console.log(response.data);
                        $location.url('/webmaster-offices');
                        localStorage.setItem('MSG', 'Your password updated successfully!');
                        userService.responseMsg($scope);
                    } else {
                        localStorage.setItem('ErrMSG', 'Your password is not updated!');
                        userService.responseMsg($scope);
                    }
                });
            }
           } 
        };
    }

    $('#summernote').summernote();
});