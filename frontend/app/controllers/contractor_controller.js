app.controller('contractor', function($scope,$window, $rootScope, $timeout, Map, userFactory, $http, $location, JOBTRACKING_CONSTANT, userService, $routeParams) {
    if (jQuery(window).width() < 768) {
        $scope.collapseClass = 'collapse';
        /*----------webmaster login factory call---------*/
    } else {
        $scope.collapseClass = '';
    }
    if (localStorage.getItem('type') != 3 && (localStorage.getItem('id') == '' || localStorage.getItem('id') == null || localStorage.getItem('id') == undefined)) {
        $location.path('/');
    }
    $scope.login_user_name = localStorage.getItem("login_user_name");
    $scope.officerLogin = function() {
        userFactory.frontendLogin($scope);
    };

    $scope.id = localStorage.getItem('id');
    if ($location.$$path == '/contractor-viewProfile' || $location.$$path == '/contractor-editProfile') {
        userFactory.viewContractors($scope, $scope.id);
    };

    /*------------- call edit contractor factory----------*/

    $scope.editContractors = function(isValid) {
        if (isValid) {
            userFactory.editContractors($scope, $scope.id);
        }
    };

    if(localStorage.getItem('profileImage')){
        $scope.service_get_image = localStorage.getItem('profileImage');
    }else{
        $scope.service_get_image = 'images/user_payment.png';
    }

    /*-------- update image ----------*/
    $('.uploadForm').submit(function (event, scope) {
        $scope.status_load = true;
        $scope.uploadId = localStorage.getItem('id');
        //stop submit the form, we will post it manually.
        event.preventDefault();
        // Get form
        var form = $(this)[0];
        var data = new FormData(form);
        $.ajax({
            type: "PUT",
            enctype: 'multipart/form-data',
            url: JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            headers: { 'Authorization': localStorage.getItem('jwt_token') },
            success: function (response) {

                $scope.service_get_image = response[0];
                localStorage.setItem('profileImage', $scope.service_get_image);
                $scope.image = true;
                if(localStorage.getItem('profileImage')){
                    $scope.service_get_image = localStorage.getItem('profileImage');
                }else{
                    alert('hello');
                    $scope.service_get_image = 'images/user_payment.png';
                }
                
                apiURL = JOBTRACKING_CONSTANT.URL + "/admin/viewuser/" + $scope.uploadId;
                 var data = {
                    profile_image:  $scope.service_get_image
                };
                $http({
                    method: 'POST',
                    url: apiURL,
                    data: data,
                    headers: { 'Authorization': localStorage.getItem('jwt_token') }
                }).then(function (response) {
                    if(response.data.status){
                       /* localStorage.setItem('MSG', 'Profile image updated successfully!');
                        userService.responseMsg($scope);*/
                    }else{
                        /*localStorage.setItem('MSG', 'Error in updated profile image!');
                        userService.responseMsg($scope);*/
                    }
                   // console.log(response);
                });
                $scope.$apply();
                jQuery('#Modeluploadphoto').modal('hide');
                jQuery('#Modeluploadphoto').find('form')[0].reset();
            },
            error: function (e) {}
        });
    });

    /*---------get all jobs factory ----------*/

    if ($location.$$path == '/contractor-jobs' || $location.$$path == '/contractor-pendingjobs') {
        userFactory.getmyJobs($scope, $scope.id);
    };

    /*---------- call check in factory ---------*/

    $scope.backpage = function() {
        window.history.back();
    }
    $scope.start = function(jobData) {
        $scope.save = false;

        jQuery('.workers_pop').modal('show');
        $scope.jobasign_id = jobData.jobasign_id;
        $scope.officeId = jobData.office_id;
        //$scope.worker = '';
        //userFactory.checkIn($scope,$scope.jobasign_id,$scope.officeId,$scope.id);
    };

    $scope.postWorkersForm = function(isValid) {
        if (isValid) {
            userFactory.checkIn($scope, $scope.jobasign_id, $scope.officeId, $scope.id);
        }
    }
    /*---------- call check in factory ---------*/

    $scope.changeStatusToActive = function(jobData) {
        $scope.dailyJobWorkId = jobData.lastrecord.id;
        userFactory.checkOut($scope, $scope.dailyJobWorkId, $scope.id);
    };

    /*-------------- Post comment on contractor profile ---------*/

    $scope.postReason = function(assignId) {
        $scope.jobAssignId = assignId;
        jQuery('#myModelnote').modal('show');
    };



    $scope.addleadNote = function(isValid) {
        if (jQuery("#add-note .note-editable").html() == '<p><br></p>') {
            jQuery('.error_label').html("Please Enter Note ").show();
            $(document).on('keyup', '#add-note .note-editable', function() {
                $('.note-editable').next('.error_label').hide('slow');
            });
        } else {
            jQuery('.error_label').html("").hide();
            userFactory.notWorkReason($scope, $scope.jobAssignId, $scope.id);
        }

    };

    if ($location.$$path == '/contractor-jobs') {
        $scope.jobsTab = true;
    };
    if ($location.$$path == '/contractor-completedjobs') {
        $scope.completejobTab = true;
    };
    if ($location.$$path == '/contractor-pendingjobs') {
        $('#summernote').summernote();
        $scope.pendingjobTab = true;
    };
    if ($location.$$path == '/contractor-workingjob') {
        $scope.workingjobTab = true;
    };

    // $scope.jobhistoryList = [];
    $scope.allJobhistory = function() {
        $scope.contatcor_id = localStorage.getItem('id');
        $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/contractorJobs/" + $scope.contatcor_id,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            // console.log(response);
            $scope.jobhistoryList = response.data;
        });
    };

    if ($location.$$path == '/contractor-alljobhistory') {
        $scope.jobhistoryTab = true;
        $scope.allJobhistory();
    }
    $scope.jobhistory = function(job) {
        localStorage.setItem("job_id", job.id);
        $location.url('/contractor-jobhistory');

    };

    function init() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 15,
            mapTypeControl: true,
            center: new google.maps.LatLng(34.051252, -84.721107),
        });

        var num_markers = locations.length;
        console.log(num_markers);
        for (var i = 0; i < num_markers; i++) {


            latlngset = new google.maps.LatLng(locations[i][2], locations[i][3]);


            var marker = new google.maps.Marker({
                map: map,
                title: locations[i][0],
                position: latlngset
            });
            map.setCenter(marker.getPosition())


            var content = "Job title: " + locations[i][0] + ',</br></h3>' + "Contractor: " + locations[i][4] + ',</br></h3>' + "Address: " + locations[i][1] +
                "</br>" + "<a href=#/officer-viewjobs/" + locations[i][5] + ">" +
                "Details</a>"

            var infowindow = new google.maps.InfoWindow()

            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));

        }
    };

    $scope.search1 = function(searchPlace) {

        if ($scope.address == undefined) {
            $scope.address = "";
        }
        if ($scope.jobCity == undefined) {
            $scope.jobCity = "";
        }
        if ($scope.jobZipcode == undefined) {
            $scope.jobZipcode = "";
        }
        if ($scope.jobState == undefined) {
            $scope.jobState = "";
        }
        $scope.address1 = $scope.address + " " + $scope.jobCity + " " + $scope.jobZipcode + " " + $scope.jobState;
        console.log($scope.address1);
        $scope.apiError = false;
        Map.search($scope.address1)
            .then(
                function(res) { // success
                    Map.addMarker(res);
                    localStorage.setItem('lat', res.geometry.location.lat());
                    localStorage.setItem('lng', res.geometry.location.lng());
                },
                function(status) { // error
                    $scope.apiError = true;
                    $scope.apiStatus = status;
                }
            );
    };
    $rootScope.jobtypeEditlistcheck = [];
    $rootScope.myobject4 = [];
    $scope.geteditJobType = function(job_id) {
        $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/assignedJobTypes/" + job_id,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobtypeEditlist = response.data;
            // console.log($scope.jobtypeEditlist);
            for (i = 0; i < $scope.jobtypeEditlist.length; i++) {
                if ($scope.jobtypeEditlist[i].status != undefined || $scope.jobtypeEditlist[i].status != null) {
                    var nd = {
                        jobName: $scope.jobtypeEditlist[i].title,
                    };
                    $rootScope.myobject4.push($scope.jobtypeEditlist[i].title);
                    $rootScope.jobtypeEditlistcheck.push(nd);
                }
            }
        });
    };

    if ($location.$$path == '/contractor-jobhistory') {
        Map.init();
        $scope.jobhistoryTab = true;
        $scope.job_id = localStorage.getItem("job_id");
        $scope.geteditJobType($scope.job_id);
        $scope.allJobhistory();
        $timeout(function() {
            console.log($scope.jobhistoryList);
            angular.forEach($scope.jobhistoryList, function(value, key) {

                if (value.id == $scope.job_id) {
                    $scope.jobList = value;
                    $scope.status = $scope.jobList.status;
                    $scope.jobStatus = "" + $scope.jobList.status;
                    $scope.operators = [{
                            value: '0',
                            displayName: 'In Progress'
                        },
                        {
                            value: '1',
                            displayName: 'Completed'
                        }, {
                            value: '2',
                            displayName: 'On Hold'
                        }
                    ]
                    if ($scope.jobList.assign.length != 0) {
                        $scope.add_newwork_jobasign_id = $scope.jobList.assign[0].id;
                        $scope.add_newwork_contractor_id = $scope.jobList.assign[0].user_id;
                        $scope.contractorIdlist = $scope.jobList.assign;
                    }
                    $scope.start_time = new Date($scope.jobList.start_date);
                    $scope.History = $scope.jobList.records;
                    $scope.jobTitle = $scope.jobList.title;
                    $scope.jobLocation = $scope.jobList.location;
                    $scope.address = $scope.jobList.address;
                    $scope.jobType = $scope.jobList.type;
                    $scope.notes = $scope.jobList.notes;
                    $scope.startDate = new Date($scope.jobList.start_date);
                    $scope.completionDate = new Date($scope.jobList.end_date);
                    $scope.jobZipcode = $scope.jobList.zip;
                    $scope.jobCity = $scope.jobList.city;
                    $scope.jobState = $scope.jobList.state;
                    $scope.officeName = String($scope.jobList.office_id);
                    $scope.amount = $scope.jobList.bid_amount;
                    // $scope.assign = String($scope.jobList.user_id);
                    $scope.search1($scope.address);
                    
                }
            });
        }, 1000);

    }
    $scope.backpage = function() {
      $window.history.back();
    };
    $scope.Userlogout = function() {
        localStorage.removeItem('id');
        localStorage.removeItem('type');
        localStorage.removeItem('login_user_name');
        $location.path('/');
    }

});