app.factory('userFactory', function(JOBTRACKING_CONSTANT, Map, $rootScope, $location, $http, userService, $routeParams) {
    var factory = {};
    /*------------ webmaster login--------*/
    factory.webmasterLogin = function($scope) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/webmaster",
            data: {
                email: $scope.usr.email,
                password: $scope.usr.password
            },
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('id', response.data.id);
                localStorage.setItem('type', response.data.type);
                localStorage.setItem('profileImage', response.data.profileImage);
                localStorage.setItem('jwt_token','JWT ' +  response.data.token);
                $location.path('/webmaster-offices');
            } else {
                $scope.responseMessage = 'Please enter a valid email id or password!';
            }
        });
    };

    /*--------------get all offices---------*/

    factory.getOfficeList = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/office",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.officeList = response.data;
        });
    };

    /*--------------get office by id---------*/

    factory.viewOffice = function($scope, officeId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/viewoffice/" + officeId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.viewOfficeList = response.data;
            $scope.officeName = response.data.name;
            $scope.officeAddress = response.data.address;
            $scope.officeDescription = response.data.description;
            $scope.phone = response.data.phone;
        });
    };

    /*-----------add office------------------*/

    factory.addOffice = function($scope) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/office",
            data: {
                name: $scope.officeName,
                address: $scope.officeAddress,
                phone: $scope.phone,
                description: $scope.officeDescription
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Office added Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-offices');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-offices');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in adding office!');
                userService.responseMsg($scope);
            }
        });
    };

    /*-----------------edit office--------------------*/

    factory.editOffice = function($scope, officeId) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/editoffice/" + officeId,
            data: {
                name: $scope.officeName,
                address: $scope.officeAddress,
                phone: $scope.phone,
                description: $scope.officeDescription
            },
           headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Office updated Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-offices');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-offices');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in office update!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------delete office--------------*/

    factory.deleteOffice = function($scope, oId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/editoffice/" + oId;
        servieContainer = this;
        return $http({
            method: 'GET',
            url: apiURL,
           headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Office deleted successfully!');
                userService.responseMsg($scope);
                servieContainer.getOfficeList($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete office!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------get all officers--------------*/
    factory.getOfficersList = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/officer",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.officersList = response.data;
        });
    };


    /*-----------add officer------------------*/

    factory.addOfficer = function($scope) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/user",
            data: {
                name: $scope.name,
                lname: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                password: $scope.password,
                type: 2,
                status: 1,
                office_id: $scope.office
            },
           headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Officer added Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-officers');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-viewprofile');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in add officer!');
                userService.responseMsg($scope);
            }
        });
    };

    /*-----------view officer------------------*/

    factory.viewOfficer = function($scope, officerId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/officer/viewofficer/" + officerId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            console.log("hdfjshjfkks", response.data[0].office_id);
            $scope.officers = response.data[0];
            $scope.name = response.data[0].name;
            $scope.lname = response.data[0].last_name;
            $scope.email = response.data[0].email;
            $scope.password = response.data[0].password;
            $scope.match_password = response.data[0].password;
            $scope.phone = response.data[0].phone;
            $scope.officers_office = String(response.data[0].office_id);

            // $scope.officers_office = response.data[2].officename;
        });
    };

    /*----------------------edit officer---------------*/

    factory.editOfficer = function($scope, officerId) {

        if ($scope.match_password != $scope.password) {
            var changePassword = 1;
        } else {
            var changePassword = 0;
        }
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/officer/viewofficer/" + officerId,
            data: {
                name: $scope.name,
                last_name: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                type: 2,
                status: 1,
                office_id: $scope.officers_office,
                changePassword: changePassword
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Officer updated successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-officers');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-viewprofile');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in update officer!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------------delete officer---------------*/

    factory.deleteOfficer = function($scope, officerDeleteId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/editofficer/" + officerDeleteId;
        servieContainer = this;
        return $http({
            method: 'GET',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getOfficersList($scope);
                localStorage.setItem('MSG', 'Officer deleted Successfully!');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete officer!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------------get all contractor---------------*/

    factory.getallContractors = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/contractor",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.contractorsList = response.data;
        });
    };


    /*----------------------get all contractor for edit job---------------*/

    $rootScope.contractorseditlist = [];
    factory.getAllcontractorsEditjob = function($scope, jobId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/assignedContractor/" + jobId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.editcontractorsList = response.data;
            // console.log($scope.editcontractorsList);
            for (i = 0; i < $scope.editcontractorsList.length; i++) {
                // $rootScope.contractorseditlist1 = [];
                if ($scope.editcontractorsList[i].status != undefined || $scope.editcontractorsList[i].status != null) {
                    var nd = {
                        contractor_id: $scope.editcontractorsList[i].id,
                        contractorName: $scope.editcontractorsList[i].name +" "+  $scope.editcontractorsList[i].last_name,
                    };
                    //  $rootScope.contractorseditlist1.push(nd);
                    $rootScope.contractorseditlist.push(nd);
                }
            };
            console.log("checked", $rootScope.contractorseditlist);

        });
    };

    /*----------------------get all office---------------*/

    factory.getallOffices = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/office",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.officeList = response.data;
        });
    };

    /*----------------------add contractor---------------*/

    factory.addContractors = function($scope) {
        if($scope.email == 'undefined' || $scope.email == undefined){
           $scope.email = ''; 
        }if($scope.password == 'undefined' || $scope.password == undefined){
            $scope.password = ''; 
        }if($scope.company == 'undefined' || $scope.company == undefined){
            $scope.company = '';
        }if($scope.crew_no == 'undefined' || $scope.crew_no == undefined){
            $scope.crew_no = '';
        }if($scope.notes == 'undefined' || $scope.notes == undefined){
           $scope.notes = '';
        }
        return $http({
         method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/user",
            data: {
                name: $scope.name,
                lname: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                password: $scope.password,
                //office_id: $scope.office,
                type: 3,
                status: 1,
                moredetails: [{ meta_key: 'company_name', meta_value: $scope.company },
                    { meta_key: 'crew_no', meta_value: $scope.crew_no },
                    { meta_key: 'notes', meta_value: $scope.notes }
                ]
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Contractor added Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-contractors');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-contractors');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in add contractor!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------------view contractor---------------*/

    factory.viewContractors = function($scope, contractorId) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/contractor",
            data: {
                id: contractorId
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
          //  console.log("meta datttatta", response.data)
            $scope.contractorDetail = response.data[0];
            $scope.name = response.data[0].name;
            $scope.lname = response.data[0].last_name;
            $scope.email = response.data[0].email;
            $scope.phone = response.data[0].phone;
            $scope.password = response.data[0].password;
            if (response.data[1].length != 0) {
                $scope.company = response.data[1][0].meta_value;
                $scope.crew_no = response.data[1][1].meta_value;
                if (response.data[1][2].length != 0) {
                    $scope.notes = response.data[1][2].meta_value;
                } else {
                    $scope.notes = null;
                }
            } else {
                $scope.company = null;
                $scope.crew_no = null;
                $scope.notes = null;
            }

            $scope.match_password = response.data[0].password;

        });
    };

    /*----------------------delete contractor---------------*/

    factory.deleteContractors = function($scope, contractorDeleteId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/editcontractor/" + contractorDeleteId;
        servieContainer = this;
        return $http({
            method: 'GET',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }

        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getallContractors($scope);
                localStorage.setItem('MSG', 'Contractor deleted Successfully!');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete contractor!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------------edit contractor---------------*/

    factory.editContractors = function($scope, contractorId) {
        if ($scope.match_password != $scope.password) {
            var changePassword = 1;
        } else {
            var changePassword = 0;
        }
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/editcontractor/" + contractorId,
            data: {
                name: $scope.name,
                last_name: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                type: 3,
                status: 1,
                company_name: $scope.company,
                crew_no: $scope.crew_no,
                notes: $scope.notes,
                changePassword: changePassword
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                 localStorage.setItem('MSG', 'Profile updated successfully!');
                 userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-contractors');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-contractors');
                }
                if (localStorage.getItem('type') == 3) {
                    $location.path('/contractor-viewProfile');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in update contractor!');
                userService.responseMsg($scope);
            }
        });
    };


    /*----------------------edit contractor---------------*/

    factory.editWebmasters = function($scope, adminId) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/editadmin/" + adminId,
            data: {
                name: $scope.name,
                last_name: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                type: 1,
                status: 1
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                 localStorage.setItem('MSG', 'Profile updated successfully!');
                 userService.responseMsg($scope);
                 $location.path('/webmaster-viewProfile');
            } else {
                localStorage.setItem('ErrMSG', 'Error in update profile!');
                userService.responseMsg($scope);
            }
        });
    };

    /*-------------get last job number factory---------------*/
    factory.getJobNumber = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/lastnumber",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobNumber = response.data.number;
            console.log($scope.jobNumber);
        });
    };
    /*-----------------add job factory---------------*/

    factory.addJob = function($scope, jobNumber) {
        $scope.number = jobNumber + 1;
        $scope.user_id = localStorage.getItem('id');
        $scope.lat = localStorage.getItem('lat');
        $scope.long = localStorage.getItem('lng');
        console.log($scope.lat);
        console.log($scope.long);
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/jobs",
            data: {
                title: $scope.jobTitle,
                location: $scope.jobLocation,
                latitude: $scope.lat,
                longitude: $scope.long,
                city: $scope.jobCity,
                state: $scope.jobState,
                zip: $scope.jobZipcode,
                address: $scope.address,
                status: '0',
                type: JSON.stringify($scope.myObject2),
                // type: $scope.jobval,
                start_date: $scope.startDate,
                end_date: $scope.completionDate,
                bid_amount: $scope.amount,
                office_id: $scope.officeName,
                user_id: $scope.user_id,
                contractors: $scope.myObject,
                notes: $scope.notes,
                number: $scope.number
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            console.log(response.data);
            if (response.data.status) {
                $scope.myObject = [];
                localStorage.setItem('MSG', 'Job added Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-job');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-jobs');
                }

            } else {
                $scope.myObject = [];
                localStorage.setItem('ErrMSG', 'Error in add job!');
                userService.responseMsg($scope);
            }
        });
    };



    factory.getallJobs = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/jobs",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobList = response.data;
        });
    };

    factory.getJobs = function($scope) {
        servieContainer = this;
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/jobstype/1",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobList = response.data;
            $scope.contractorStatus = false;
            console.log($scope.jobList);
        });
    };

    factory.getarchiveJobs = function($scope) {
        servieContainer = this;
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/jobstype/0",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobList = response.data;
        });
    };

    factory.viewJobs = function($scope, jobId) {
        servieContainer = this;
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/viewjob/" + jobId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            // console.log(response.data);
            $scope.status = response.data.status;
            $scope.jobStatus = "" + response.data.status;


            $scope.operators = [{
                    value: '0',
                    displayName: 'In Progress'
                },
                {
                    value: '1',
                    displayName: 'Completed'
                }, {
                    value: '2',
                    displayName: 'On Hold'
                }
            ]

            $scope.jobList = response.data;
            if (response.data.assign.length != 0) {
                $scope.add_newwork_jobasign_id = response.data.assign[0].id;
                $scope.add_newwork_contractor_id = response.data.assign[0].user_id;
                $scope.contractorIdlist = response.data.assign;
            }
            // $scope.add_newwork_contractor_name =  response.data.records[0].name;
            $scope.start_time = new Date(response.data.start_date);
            // $scope.end_time =  new Date(response.data.end_date);
            $scope.History = response.data.records;
            $scope.jobTitle = response.data.title;
            $scope.jobLocation = response.data.location;
            $scope.address = response.data.address;
            $scope.jobType = response.data.type;
            $scope.notes = response.data.notes;
            $scope.startDate = new Date(response.data.start_date);
            $scope.completionDate = new Date(response.data.end_date);
            $scope.jobZipcode = response.data.zip;
            $scope.jobCity = response.data.city;
            $scope.jobState = response.data.state;
            $scope.officeName = String(response.data.office_id);
            $scope.amount = response.data.bid_amount;
            // $scope.assign = String(response.data.user_id);
            $scope.search1($scope.address);


        });
    };


    $rootScope.locations = [];
    factory.viewActiveJobs = function($scope) {

        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/activejobs",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $rootScope.activejobList = response.data;
            for (i = 0; i < $rootScope.activejobList.length; i++) {
                $rootScope.location1 = [];
                if ($rootScope.activejobList[i].job != undefined || $rootScope.activejobList[i].job != null) {
                    $rootScope.location1.push($rootScope.activejobList[i].job.title, $rootScope.activejobList[i].job.address, $rootScope.activejobList[i].job.latitude, $rootScope.activejobList[i].job.longitude, $rootScope.activejobList[i].contractor_name, $rootScope.activejobList[i].job.id);
                    $rootScope.locations.push($rootScope.location1);
                }
            };
        });

    };

    $rootScope.Joblocations = [];
    factory.viewAllJobsmap = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/jobs",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $rootScope.jobList = response.data;
            for (i = 0; i < $rootScope.jobList.length; i++) {
                $rootScope.location2 = [];
                if (($rootScope.jobList[i].title != undefined || $rootScope.jobList[i].title != null) && ($rootScope.jobList[i].address != undefined || $rootScope.jobList[i].address != null) && ($rootScope.jobList[i].latitude != undefined || $rootScope.jobList[i].latitude != null) && ($rootScope.jobList[i].longitude != undefined || $rootScope.jobList[i].longitude != null) && ($rootScope.jobList[i].contractor_name != undefined || $rootScope.jobList[i].contractor_name != null)) {
                    $rootScope.location2.push($rootScope.jobList[i].title, $rootScope.jobList[i].address, $rootScope.jobList[i].latitude, $rootScope.jobList[i].longitude, $rootScope.jobList[i].contractor_name, $rootScope.jobList[i].id);
                    $rootScope.Joblocations.push($rootScope.location2);
                }
            };
        });

    };

    factory.editJobs = function($scope, jobId) {
        $scope.lat = localStorage.getItem('lat');
        $scope.long = localStorage.getItem('lng');
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/editjobs/" + jobId,
            data: {
                title: $scope.jobTitle,
                location: $scope.jobLocation,
                latitude: $scope.lat,
                longitude: $scope.long,
                city: $scope.jobCity,
                state: $scope.jobState,
                zip: $scope.jobZipcode,
                address: $scope.address,
                status: $scope.jobStatus,
                type: $scope.jobType,
                start_date: $scope.startDate,
                end_date: $scope.completionDate,
                bid_amount: $scope.amount,
                office_id: $scope.officeName,
                contractors: $rootScope.contractorseditlist,
                notes: $scope.notes
            },
          headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Job updated Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-job');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-jobs');
                }
            } else {
                localStorage.setItem('ErrMSG', 'Error in update job!');
                userService.responseMsg($scope);
            }
        });
    };
    
    factory.deleteJobs = function($scope, jobId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/jobs/deleteJob/" + jobId;
        servieContainer = this;
        return $http({
            method: 'delete',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getJobs($scope);
                localStorage.setItem('MSG', response.data.msg);
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', response.data.msg);
                userService.responseMsg($scope);
            }
        });
    };

    factory.archiveJobs = function($scope, jobId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/archiveJob/" + jobId;
        servieContainer = this;
        return $http({
            method: 'PUT',
            url: apiURL,
            data: {
                status: 2
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getJobs($scope);
                localStorage.setItem('MSG', response.data.msg);
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', response.data.msg);
                userService.responseMsg($scope);
            }
        });
    };
   
    factory.resumeJobs = function($scope, jobId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/archiveJob/" + jobId;
        servieContainer = this;
        return $http({
            method: 'PUT',
            url: apiURL,
            data: {
                status: 0
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getarchiveJobs($scope);
                localStorage.setItem('MSG', response.data.msg);
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', response.data.msg);
                userService.responseMsg($scope);
            }
        });
    };

    factory.frontendLogin = function($scope) {
        $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/officer/signin",
            data: {
                email: $scope.usr.email,
                password: $scope.usr.password
            },
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('id', response.data.id);
                localStorage.setItem('type', response.data.type);
                localStorage.setItem('profileImage', response.data.profileImage);
                localStorage.setItem('jwt_token', 'JWT ' + response.data.token);
               
                localStorage.setItem('login_user_name', response.data.name.split(' ')[0]);
                if (localStorage.getItem('type') == 2) {
                    $scope.type = 2;
                    $location.path('/officer-jobs');
                }
                if (localStorage.getItem('type') == 3) {
                    $location.path('/contractor-pendingjobs');
                }
            } else {
                $scope.responseMessage = 'Please enter a valid email id or password!';
            }
        });
    };

    /*--------------get my jobs -----------*/

    factory.getmyJobs = function($scope, jobId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/contractor/myjobs/" + jobId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobList = response.data;
        });
    };

    /*---------------add job type-------------*/
    factory.addJobType = function($scope) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/jobType",
            data: {
                title: $scope.jobtype
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                $location.path("/webmaster-jobtype");
                localStorage.setItem('MSG', 'Job Type added Successfully!');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in add job type!');
                userService.responseMsg($scope);
            }
        });
    };
    /*---------------get job type-----------*/
    factory.getJobType = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/jobType",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.jobtypeList = response.data;
            $scope.materialss = response.data;
            angular.forEach($scope.materialss, function(obj) {
                obj["checked"] = false;
                obj.checked = false;
            });
            // $scope.jobtype = response.data[0].title;
            // $scope.jobtypes = response.data[0].id;
        });
    };

    $rootScope.jobtypeEditlistcheck = [];
    $rootScope.myobject4 = [];
    factory.geteditJobType = function($scope,job_id) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/assignedJobTypes/"+job_id,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
             $scope.jobtypeEditlist = response.data;
            for (i = 0; i < $scope.jobtypeEditlist.length; i++) {
                if ($scope.jobtypeEditlist[i].status != undefined || $scope.jobtypeEditlist[i].status != null) {
                   var nd = {
                            jobName: $scope.jobtypeEditlist[i].title,
                        };
                    $rootScope.myobject4.push($scope.jobtypeEditlist[i].title);  
                    $rootScope.jobtypeEditlistcheck.push(nd);
                }
            }
        });
    };
   
    /*-------------edit job type --------------*/
    factory.editJobType = function($scope, jobTypeId) {
        return $http({
            method: 'PUT',
            url: JOBTRACKING_CONSTANT.URL + "/admin/jobType/" + jobTypeId,
            data: {
                title: $scope.jobtype
            },
         headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                $location.path("/webmaster-jobtype");
                localStorage.setItem('MSG', 'Job type updated Successfully!');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in update job type!');
                userService.responseMsg($scope);
            }
        });
    };

    /*------------delete job type-----------------*/

    factory.deleteJobType = function($scope, jobTypeDeleteId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/jobType/" + jobTypeDeleteId;
        servieContainer = this;
        return $http({
            method: 'DELETE',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getJobType($scope);
                localStorage.setItem('MSG', 'Job type deleted Successfully !');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete job type!');
                userService.responseMsg($scope);
            }
        });
    };
    /*-----------check in system--------------*/

    factory.checkIn = function($scope, jobId, officeId, contractorId) {
        $scope.save = true;
        servieContainer = this;
        $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/clockin",
            data: {
                jobasign_id: jobId,
                officeid: officeId,
                contractor_id: contractorId,
                crew_count: $scope.worker
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                jQuery('.workers_pop').modal('hide');
                localStorage.setItem('MSG', 'number of workers added Successfully!');
                userService.responseMsg($scope);
                servieContainer.getmyJobs($scope, contractorId);
            } else {
                jQuery('.workers_pop').modal('hide');
                localStorage.setItem('ErrMSG', 'Error in add number of workers!');
                userService.responseMsg($scope);
            }
        });
    };


    /*-----------checkout system--------------*/

    factory.checkOut = function($scope, dailyJobWorkId, contractorId) {
        servieContainer = this;
        $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/clockout",
            data: {
                id: dailyJobWorkId
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getmyJobs($scope, contractorId);
            } else {

            }
        });
    };

    /*---------------assign jobs factory-------------*/

    factory.assignJobs = function($scope, jobId) {
        $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/editjobs",
            data: {
                user_id: $scope.user_id,
                job_id: $scope.user_id,
                date_alloted: $scope.alloted_date,
                date_completed: $scope.completed_date,
                status: 1,
                comments: $scope.comment
            },
          headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {

            } else {

            }
        });
    };

    /*----------------not worked reason factory-------------*/

    factory.notWorkReason = function($scope, jobAssignId, contractorId) {
        $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/notworked",
            data: {
                reason: jQuery("#add-note .note-editable").html(),
                jobasign_id: jobAssignId,
                contractor_id: contractorId
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                jQuery('#myModelnote').modal('hide');
                localStorage.setItem('MSG', 'Note submitted Successfully!');
                userService.responseMsg($scope);
            } else {
                jQuery('#myModelnote').modal('hide');
                localStorage.setItem('ErrMSG', 'Error in submit Note!');
                userService.responseMsg($scope);
            }
        });
    };

    /*------------- get job by job type------------*/

    factory.getJobByType = function($scope, jobType) {

        if ($scope.contractortypes) {
            $http({
                method: 'POST',
                url: JOBTRACKING_CONSTANT.URL + "/jobs/filter",
                data: {
                    contractor_id: $scope.contractortypes,
                    jobtype: jobType
                },
             headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                $scope.jobList = response.data;

            });
        } else {
            $http({
                method: 'POST',
                url: JOBTRACKING_CONSTANT.URL + "/jobs/filter",
                data: {
                    jobtype: jobType
                },
             headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                $scope.jobList = response.data;
                if (response.data.status) {

                } else {

                }
            });
        }
    };

    /*------------- get job by job type------------*/

    factory.getJobByContractor = function($scope, contractorId) {
        if ($scope.types) {
            $http({
                method: 'POST',
                url: JOBTRACKING_CONSTANT.URL + "/jobs/filter",
                data: {
                    jobtype: $scope.types,
                    contractor_id: contractorId
                },
                headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                $scope.jobList = response.data;

            });
        } else {
            $http({
                method: 'POST',
                url: JOBTRACKING_CONSTANT.URL + "/jobs/filter",
                data: {
                    contractor_id: contractorId
                },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
                $scope.jobList = response.data;
                $scope.contractorStatus = true;
                console.log($scope.jobList);
            });
        }
    };

    factory.mapSearch = function($scope) {
        $scope.address1 = 'center on acworth, georgia 30101';
        Map.search($scope.address1)
            .then(
                function(res) { // success
                    Map.addMarker(res);
                    localStorage.setItem('lat', res.geometry.location.lat());
                    localStorage.setItem('lng', res.geometry.location.lng());
                },
                function(status) { // error
                    $scope.apiError = true;
                    $scope.apiStatus = status;
                }
            );
    };
    /*-------------------code for admin---------------------------------*/

    /*----------------get all admin--------------*/
    factory.getAdminList = function($scope, adminId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/getadmin/" + adminId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.adminList = response.data;
        });
    };

    /*-------- view admin ----------*/
     factory.viewWebmaster = function($scope, adminId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/admin/" + adminId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.adminList = response.data;
            $scope.name = response.data.name;
            $scope.lname = response.data.last_name;
            $scope.email = response.data.email;
            $scope.phone = response.data.phone;
            $scope.service_get_image = response.data.profile_image;        
        });
    };

    /*----------------------view admin---------------*/

    factory.viewAdmins = function($scope, adminId) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/admin/" + adminId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            $scope.name = response.data.name;
            $scope.lname = response.data.last_name;
            $scope.email = response.data.email;
            $scope.phone = response.data.phone;
            $scope.created_at = response.data.created_at;
            $scope.password = response.data.password;
            $scope.match_password = response.data.password;
        });
    };

    factory.getAdminPassword = function($scope, adminId) {

        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/admin/" + adminId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }


        }).then(function(response) {
            $scope.email = response.data.email;
            $scope.cpass = response.data.password;
        });
    };

    /*----------------------delete Admimn---------------*/

    factory.deleteAdmins = function($scope, AdminDeleteId) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/admin/editadmin/" + AdminDeleteId;
        servieContainer = this;
        return $http({
            method: 'GET',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }

        }).then(function(response) {
            if (response.data.status) {
                servieContainer.getAdminList($scope);
                localStorage.setItem('MSG', 'Admin deleted Successfully!');
                userService.responseMsg($scope);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete Admin!');
                userService.responseMsg($scope);
            }
        });
    };
    /*----------------------add Admin---------------*/

    factory.addAdmins = function($scope) {
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/user",
            data: {
                name: $scope.name,
                lname: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                password: $scope.password,
                type: 1,
                status: 1,
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Contractor added Successfully!');
                userService.responseMsg($scope);
                $location.path('/webmaster-admins');
            } else {
                localStorage.setItem('ErrMSG', 'Error in add contractor!');
                userService.responseMsg($scope);
            }
        });
    };
    /*----------------------Edit Admin---------------*/

    factory.editAdmin = function($scope, adminId) {
        if ($scope.match_password != $scope.password) {
            var changePassword = 1;
        } else {
            var changePassword = 0;
        }

        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/editadmin/" + adminId,
            data: {
                name: $scope.name,
                last_name: $scope.lname,
                email: $scope.email,
                phone: $scope.phone,
                type: 1,
                status: 1,
                changePassword: changePassword
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
            }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Admin updated Successfully!');
                userService.responseMsg($scope);
                $location.path('/webmaster-admins');
            } else {
                localStorage.setItem('ErrMSG', 'Error in update Admin!');
                userService.responseMsg($scope);
            }
        });
    };

    /*----------------------function for  add update get leads ----------------------------------*/
    factory.addLead = function($scope) {
       
        // $scope.user_id = localStorage.getItem('id');
        if($scope.notes){
           $scope.notess = $scope.notes; 
        }else{
            $scope.notess = '';
        } if($scope.source){
            $scope.sources = $scope.source;
        }else{
            $scope.sources = '';
        }
        $scope.lat = localStorage.getItem('lat');
        $scope.long = localStorage.getItem('lng');
        
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/lead ",
            data: {
                name: $scope.lead_name,
                lat: $scope.lat,
                lng: $scope.long,
                city: $scope.jobCity,
                state: $scope.jobState,
                zip: $scope.jobZipcode,
                address: $scope.address,
                email: $scope.email,
                officer_id: $scope.officer_id,
                contact_no: $scope.phone,
                note: $scope.notess,
                source : $scope.sources
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                $scope.myObject = [];
                $scope.finalstatus = false;
                $scope.lead_name = '';
                $scope.lead_id = '';
                $scope.jobCity = '';
                $scope.jobState = '';
                $scope.jobZipcode = '';
                $scope.created_at = '';
                $scope.address = '';
                $scope.email = '';
                $scope.officer_id = '';
                $scope.match_officer_id = '';
                $scope.officer_name = '';
                $scope.phone = '';
                $scope.notesList = '';
                $scope.source  = ''; 
                if (localStorage.getItem('type') == 1) {
                    localStorage.setItem('MSG', 'Lead added Successfully!');
                    userService.responseMsg($scope);
                    $location.path('/webmaster-leads');
                }
                if (localStorage.getItem('type') == 2) {
                    localStorage.setItem('MSG', 'Lead added Successfully!');
                    userService.responseMsg($scope);
                    $location.path('/officer-leads');
                } else {
                    localStorage.setItem('MSG', 'Lead submitted Successfully!');
                    userService.responseMsg($scope);
                }

            } else {
                $scope.myObject = [];
                localStorage.setItem('ErrMSG', 'Error in add lead!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.viewLead = function($scope, leadId) {
        servieContainer = this;
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/lead/" + leadId,
             headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {

            $scope.lead_name = response.data.name;
            $scope.lead_id = response.data.id;
            $scope.jobCity = response.data.city;
            $scope.jobState = response.data.state;
            $scope.jobZipcode = response.data.zip;
            $scope.created_at = response.data.created_at;
            $scope.address = response.data.address;
            $scope.email = response.data.email;
            $scope.officer_id = String(response.data.officer_id);
            $scope.match_officer_id = String(response.data.officer_id);
            $scope.officer_name = response.data.officerName;
            $scope.phone = response.data.contact_no;
            $scope.notesList = response.data.notes;
            $scope.search1($scope.address);
            $scope.source  = (response.data.source == null)? '': String(response.data.source);
        });
    };

    factory.editLead = function($scope) {
        // $scope.user_id = localStorage.getItem('id');
        
        $scope.lat = localStorage.getItem('lat');
        $scope.long = localStorage.getItem('lng');
        var status = false;
        if ($scope.match_officer_id != $scope.officer_id) {
            var status = true;
        }
        return $http({
            method: 'PUT',
            url: JOBTRACKING_CONSTANT.URL + "/admin/lead",
            data: {
                id: $scope.lead_id,
                name: $scope.lead_name,
                lat: $scope.lat,
                lng: $scope.long,
                city: $scope.jobCity,
                state: $scope.jobState,
                zip: $scope.jobZipcode,
                address: $scope.address,
                email: $scope.email,
                officer_id: $scope.officer_id,
                contact_no: $scope.phone,
                status: status,
                source : $scope.source
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
         }).then(function(response) {
            if (response.data.status) {
                $scope.myObject = [];
                localStorage.setItem('MSG', 'Lead edited Successfully!');
                userService.responseMsg($scope);
                if (localStorage.getItem('type') == 1) {
                    $location.path('/webmaster-leads');
                }
                if (localStorage.getItem('type') == 2) {
                    $location.path('/officer-leads');
                }

            } else {
                $scope.myObject = [];
                localStorage.setItem('ErrMSG', 'Error in edit lead!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.getallLead = function($scope) {
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/admin/lead",
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status != false) {
                $scope.leadList = response.data;
            }
        });
    };
    /*----------------------for note edit update ----------------------------------*/
    factory.viewNotes = function($scope, jobId) {
        servieContainer = this;
        return $http({
            method: 'GET',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/notes/" + jobId,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }

        }).then(function(response) {
            $scope.notesList = response.data;
        });
    };

    factory.addNotes = function($scope, userId, jobId) {

        console.log(userId, jobId, jQuery("#add-note .note-editable").html());
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/notes",
            data: {
                user_id: userId,
                job_id: jobId,
                note: jQuery("#add-note .note-editable").html(),
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }
   
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Note added Successfully!');
                userService.responseMsg($scope);
                jQuery('#myModelnote').modal('hide');
                servieContainer.viewNotes($scope, jobId);
            } else {
                localStorage.setItem('ErrMSG', 'Error in Note add!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.addleadNotes = function($scope, userId, leadId) {
        console.log(userId, leadId, jQuery("#add-note .note-editable").html());
        return $http({
            method: 'POST',
            url: JOBTRACKING_CONSTANT.URL + "/admin/note",
            data: {
                user_id: userId,
                lead_id: leadId,
                note: jQuery("#add-note .note-editable").html(),
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Note added Successfully!');
                userService.responseMsg($scope);
                jQuery('#myModelnote').modal('hide');
                jQuery('#myModelnote').find("input").val('');
                servieContainer.viewLead($scope, leadId);
            } else {
                localStorage.setItem('ErrMSG', 'Error in Note add!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.editNotesave = function($scope) {
        return $http({
            method: 'PUT',
            url: JOBTRACKING_CONSTANT.URL + "/jobs/notes",
            data: {
                id: $scope.edit_note_id,
                note: jQuery("#edit_note .note-editable").html(),
            },
        headers: { 'Authorization': localStorage.getItem('jwt_token') }
 
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Note edited Successfully!');
                userService.responseMsg($scope);
                servieContainer.viewNotes($scope, $scope.edit_job_id);
                jQuery('#ModeleditComment').modal('hide');
            } else {
                localStorage.setItem('ErrMSG', 'Error in Notes edit!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.editleadNotesave = function($scope) {
        return $http({
            method: 'PUT',
            url: JOBTRACKING_CONSTANT.URL + "/admin/note",
            data: {
                id: $scope.edit_note_id,
                note: jQuery("#edit_note .note-editable").html(),
            },
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                localStorage.setItem('MSG', 'Note edited Successfully!');
                userService.responseMsg($scope);
                servieContainer.viewLead($scope, $scope.edit_lead_id);
                jQuery('#ModeleditComment').modal('hide');
            } else {
                localStorage.setItem('ErrMSG', 'Error in Notes edit!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.deletenote = function($scope, note_ID, job_id) {
        apiURL = JOBTRACKING_CONSTANT.URL + "/jobs/notes/" + note_ID;
        servieContainer = this;
        return $http({
            method: 'DELETE',
            url: apiURL,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                jQuery('#ModelDeleteComment').modal('hide');      
                localStorage.setItem('MSG', 'Note deleted successfully!');
                userService.responseMsg($scope);
                servieContainer.viewNotes($scope, job_id);
            } else {
                localStorage.setItem('ErrMSG', 'Error in delete Note!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.editJobtime = function($scope, edittimeId, jobId) {
        var editstartdate = moment($scope.edit_start_date).format("L") + " " + moment($scope.editStartdate).format("HH:mm");
        var editStartDate = new Date(editstartdate);  
       
       if($scope.edit_end_date == "" || $scope.editEnddate == null){      
           var editEndDate = null;
           var activestatus = 1; 
        }else{
              var editenddate = moment($scope.edit_end_date).format("L") + " " + moment($scope.editEnddate).format("HH:mm");
               var editEndDate = new Date(editenddate);  
               var activestatus = 0; 
        }        
                    
        var data = {
            created_at: editStartDate,
            updated_at: editEndDate ,
            contractor_id : $scope.edit_contractor_id,
            crew_count : $scope.edit_no_worker,         
            active: activestatus
        };

         apiURL = JOBTRACKING_CONSTANT.URL + "/jobs/timeJob/" + edittimeId;
        $http({
            method: 'PUT',
            url: apiURL,
            data: data,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                jQuery('#editJobtime').modal('hide');
                localStorage.setItem('MSG', 'Time edited  successfully!');
                userService.responseMsg($scope);
                servieContainer.viewJobs($scope, jobId);
                servieContainer.viewNotes($scope, jobId);
            } else {
                jQuery('#editJobtime').modal('hide');
                localStorage.setItem('MSG', 'Error in time edit!');
                userService.responseMsg($scope);
            }
        });
    };

    factory.addJobHistory = function($scope, jobId) {
        var startDate = moment($scope.start_date).format("L") + " " + moment($scope.start_time).format("HH:mm:ss");
        if (($scope.end_date != null || $scope.end_date != "Invalid Date") && $scope.end_time != null) {
            var endDate = moment($scope.end_date).format("L") + " " + moment($scope.end_time).format("HH:mm:ss");
            var enddate = moment(endDate).format();
            $scope.active = 0;
        }

        if (($scope.end_date == null || $scope.end_date == "Invalid Date") && $scope.end_time != null) {
            var endDate = moment($scope.end_time).format("HH:mm:ss");
            var enddate = moment(endDate).format();
            $scope.active = 0;
        }
        if (($scope.end_date != null || $scope.end_date != "Invalid Date") && $scope.end_time == null) {
            var endDate = moment($scope.end_date).format("L");
            var enddate = moment(endDate).format();
            $scope.active = 0;

        }

        if (($scope.end_date == null || $scope.end_date == "Invalid Date") && $scope.end_time == null) {
            var enddate = null;
            $scope.active = 1;
        }
        var startdate = moment(startDate).format();

        var data = {
            contractor_id: $scope.contractor_id,
            crew_count: $scope.no_worker,
            created_at: startdate,
            updated_at: enddate,
            updated_at: enddate,
            active: $scope.active,
        };
        apiURL = JOBTRACKING_CONSTANT.URL + "/jobs/timeJob/" + $scope.add_newwork_jobasign_id;
        $http({
            method: 'POST',
            url: apiURL,
            data: data,
            headers: { 'Authorization': localStorage.getItem('jwt_token') }
        }).then(function(response) {
            if (response.data.status) {
                jQuery('#myModeljobHistory').modal('hide');
                $('#myModeljobHistory').find("input").val('');
                localStorage.setItem('MSG', 'Job History added  successfully!');
                userService.responseMsg($scope);
                servieContainer.viewJobs($scope, jobId);
                servieContainer.viewNotes($scope, jobId);
            } else {
                jQuery('#myModeljobHistory').modal('hide');
                $('#myModeljobHistory').find("input").val('');
                localStorage.setItem('MSG', 'Error in add Job  History!');
                userService.responseMsg($scope);
            }
        });
    };

    return factory;
}).run(function() {


});
//});